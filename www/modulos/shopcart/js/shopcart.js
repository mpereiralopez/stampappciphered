(function () {

  var app = angular.module('app.shopcart.controller', []);

  app.controller('shopcartController', function ($scope, $http, basedatos, consultasbd, datos) {
    var shopcart = this;
    shopcart.isOnline = true;

    isOnline(
      function () {
        //alert("Sorry, we currently do not have Internet access.");
        shopcart.isOnline = false;
      },
      function () {
        //alert("Succesfully connected!");
        shopcart.isOnline = true;
      }
    );


    shopcart.apksArray = [];
    var plat = device.platform;

    if (plat == "iOS") {
      var URL = "https://itunes.apple.com/lookup?id=1219800948&entity=software";
      $.ajax({
        url: URL,
        success: function (result) {
          var resultObj = JSON.parse(result);
          var arrayOfResults = resultObj.results;
          for (var i = 0; i < arrayOfResults.length; i++) {
            var currentApp = arrayOfResults[i];
            if (currentApp && currentApp.kind == 'software') {
              var apkObj = {};
              apkObj.appId = currentApp.trackId;
              apkObj.price = currentApp.price;
              apkObj.title = currentApp.trackName
              apkObj.summary = currentApp.description
              apkObj.developer = currentApp.artistName
              apkObj.icon = currentApp.artworkUrl100
              apkObj.url = currentApp.trackViewUrl
              apkObj.score = currentApp.averageUserRating
              shopcart.apksArray.push(apkObj)
            }
          }
        }
      });

    } else {
      //Android
      var baseURL = "https://play.google.com";
      var url = baseURL + "/store/apps/developer?id=Gloeasy";
      $.ajax({
        url: url,
        success: function (result) {
          var response = $(result);
          var apks = [];
          apks = $(result).find(".card");
          for (var i = 0; i < apks.length; i++) {
            var apkObj = {};
            var apk = $(apks[i]);
            apkObj.appId = apk.attr('data-docid');
            var price = apk.find('span.display-price').first().text();
            apkObj.price = (price != '') ? price : '0';
            apkObj.title = apk.find('a.title').attr('title');
            apkObj.summary = apk.find('div.description').text().trim();
            apkObj.developer = apk.find('a.subtitle').text();
            apkObj.icon = 'https:' + apk.find('img.cover-image').attr('data-cover-large');
            apkObj.url = baseURL + apk.find('a').attr('href');
            var scoreText = apk.find('div.tiny-star').attr('aria-label');
            if (scoreText) {
              apkObj.score = parseFloat(scoreText.match(/[\d.]+/)[0]);
            } else {
              apkObj.score = '0';
            }
            shopcart.apksArray.push(apkObj)
            //console.log(apkObj);
          }

        }
      });
    }




    function isOnline(no, yes) {
      var xhr = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHttp');
      xhr.onload = function () {
        if (yes instanceof Function) {
          yes();
        }
      }
      xhr.onerror = function () {
        if (no instanceof Function) {
          no();
        }
      }
      xhr.open("GET", "http://google.es", true);
      xhr.send();
    }

  });

})();
