'use strict';

(function () {
  var app = angular.module('app.poi.controller', []);

  app.controller('POIController', function ($scope) {
    console.log('POIController.js');
    var poi = this
    var onSuccess = function (position) {

      console.log(position);
      /*alert('Latitude: ' + position.coords.latitude + '\n' +
        'Longitude: ' + position.coords.longitude + '\n' +
        'Altitude: ' + position.coords.altitude + '\n' +
        'Accuracy: ' + position.coords.accuracy + '\n' +
        'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
        'Heading: ' + position.coords.heading + '\n' +
        'Speed: ' + position.coords.speed + '\n' +
        'Timestamp: ' + position.timestamp + '\n');*/

     
      var map;
      $.getScript("http://maps.googleapis.com/maps/api/js?v=3&sensor=true", function () {

        map = new google.maps.Map(document.getElementById('map'), {
          center: { lat: position.coords.latitude, lng:  position.coords.longitude },
          zoom: 12
        });

        var customMarker = new google.maps.Marker({
          position: new google.maps.LatLng(position.coords.latitude,  position.coords.longitude),
          map: map
        });

      });

    };

    // onError Callback receives a PositionError object
    //
    function onError(error) {
      alert('code: ' + error.code + '\n' +
        'message: ' + error.message + '\n');
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError);

  });

})();
