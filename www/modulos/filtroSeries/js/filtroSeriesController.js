'use strict';

(function () {
  var app = angular.module('app.filtroSeries.controller', []);

  app.controller('filtroSeriesController', function ($log, $scope, $q, $filter, $uibModal, datos, consultasbd) {
    console.log('filtroSeriesController.js');

    var filtroSeries = this;

    /** VER SI ES PARCIAL O TOTAL **/
    filtroSeries.tipoColeccion = $scope.$parent.main.tipoColeccion;

    filtroSeries.textoFormulario = $filter('translate')('TXTFORMTEMA_SERIE');
    //console.log($translate('TXTFORMTEMA'));

    filtroSeries.vectorSellosMostrar = [];
    filtroSeries.vectorSellosMostrar.totalSeries = 0;

    filtroSeries.buscarSellosCategoriaGeneric = function (themeValue) {

      if (filtroSeries.tipoColeccion == 'parcial') {

        consultasbd.getColeccionSeriesOfUser(datos.getActualTableCollection(), themeValue).then(
          function (data) {
            console.log(data);
            filtroSeries.vectorSellosMostrar = data;
            //datos.setLocalStorage('txtUltimaBusquedaSeries',$scope.selected);
          },
          function (err) {
            console.log(err);
          }
        )
      } else {
        consultasbd.getColeccionSeries(datos.getActualTableCollection(), themeValue).then(
          function (data) {
            console.log(data);
            filtroSeries.vectorSellosMostrar = data;
            datos.setLocalStorage('txtUltimaBusquedaSeries', $scope.selected);
          },
          function (err) {
            console.log(err);
          }
        )
      }
    };


    filtroSeries.buscarSellosCategoria = function () {

      filtroSeries.buscarSellosCategoriaGeneric($scope.selected);
    };

    filtroSeries.limpiarSellosCategoria = function () {
      $scope.selected = '';
      datos.setLocalStorage('txtUltimaBusquedaSeries', '');
    };

    filtroSeries.ocultarSoftkeys = function () {
      datos.ocultarSoftkeys();
    };

    filtroSeries.addSello = function (indice) {
      filtroSeries.vectorSellosMostrar.series[indice].cantidad++;
      consultasbd.setColeccionUsuario(filtroSeries.vectorSellosMostrar.series[indice].Id, filtroSeries.vectorSellosMostrar.series[indice].cantidad);
    };

    filtroSeries.remSello = function (indice) {
      filtroSeries.vectorSellosMostrar.series[indice].cantidad--;
      if (filtroSeries.vectorSellosMostrar.series[indice].cantidad <= 0) {
        filtroSeries.vectorSellosMostrar.series[indice].cantidad = 0;
        consultasbd.setColeccionUsuario(filtroSeries.vectorSellosMostrar.series[indice].Id, filtroSeries.vectorSellosMostrar.series[indice].cantidad);
      }
    };

    if (filtroSeries.tipoColeccion != 'parcial') $scope.selected = datos.getLocalStorage('txtUltimaBusquedaSeries');

    //$scope.statesWithFlags = [{'name':'Alabama','flag':'5/5c/Flag_of_Alabama.svg/45px-Flag_of_Alabama.svg.png'},{'name':'Alaska','flag':'e/e6/Flag_of_Alaska.svg/43px-Flag_of_Alaska.svg.png'},{'name':'Arizona','flag':'9/9d/Flag_of_Arizona.svg/45px-Flag_of_Arizona.svg.png'},{'name':'Arkansas','flag':'9/9d/Flag_of_Arkansas.svg/45px-Flag_of_Arkansas.svg.png'},{'name':'California','flag':'0/01/Flag_of_California.svg/45px-Flag_of_California.svg.png'},{'name':'Colorado','flag':'4/46/Flag_of_Colorado.svg/45px-Flag_of_Colorado.svg.png'},{'name':'Connecticut','flag':'9/96/Flag_of_Connecticut.svg/39px-Flag_of_Connecticut.svg.png'},{'name':'Delaware','flag':'c/c6/Flag_of_Delaware.svg/45px-Flag_of_Delaware.svg.png'},{'name':'Florida','flag':'f/f7/Flag_of_Florida.svg/45px-Flag_of_Florida.svg.png'},{'name':'Georgia','flag':'5/54/Flag_of_Georgia_%28U.S._state%29.svg/46px-Flag_of_Georgia_%28U.S._state%29.svg.png'},{'name':'Hawaii','flag':'e/ef/Flag_of_Hawaii.svg/46px-Flag_of_Hawaii.svg.png'},{'name':'Idaho','flag':'a/a4/Flag_of_Idaho.svg/38px-Flag_of_Idaho.svg.png'},{'name':'Illinois','flag':'0/01/Flag_of_Illinois.svg/46px-Flag_of_Illinois.svg.png'},{'name':'Indiana','flag':'a/ac/Flag_of_Indiana.svg/45px-Flag_of_Indiana.svg.png'},{'name':'Iowa','flag':'a/aa/Flag_of_Iowa.svg/44px-Flag_of_Iowa.svg.png'},{'name':'Kansas','flag':'d/da/Flag_of_Kansas.svg/46px-Flag_of_Kansas.svg.png'},{'name':'Kentucky','flag':'8/8d/Flag_of_Kentucky.svg/46px-Flag_of_Kentucky.svg.png'},{'name':'Louisiana','flag':'e/e0/Flag_of_Louisiana.svg/46px-Flag_of_Louisiana.svg.png'},{'name':'Maine','flag':'3/35/Flag_of_Maine.svg/45px-Flag_of_Maine.svg.png'},{'name':'Maryland','flag':'a/a0/Flag_of_Maryland.svg/45px-Flag_of_Maryland.svg.png'},{'name':'Massachusetts','flag':'f/f2/Flag_of_Massachusetts.svg/46px-Flag_of_Massachusetts.svg.png'},{'name':'Michigan','flag':'b/b5/Flag_of_Michigan.svg/45px-Flag_of_Michigan.svg.png'},{'name':'Minnesota','flag':'b/b9/Flag_of_Minnesota.svg/46px-Flag_of_Minnesota.svg.png'},{'name':'Mississippi','flag':'4/42/Flag_of_Mississippi.svg/45px-Flag_of_Mississippi.svg.png'},{'name':'Missouri','flag':'5/5a/Flag_of_Missouri.svg/46px-Flag_of_Missouri.svg.png'},{'name':'Montana','flag':'c/cb/Flag_of_Montana.svg/45px-Flag_of_Montana.svg.png'},{'name':'Nebraska','flag':'4/4d/Flag_of_Nebraska.svg/46px-Flag_of_Nebraska.svg.png'},{'name':'Nevada','flag':'f/f1/Flag_of_Nevada.svg/45px-Flag_of_Nevada.svg.png'},{'name':'New Hampshire','flag':'2/28/Flag_of_New_Hampshire.svg/45px-Flag_of_New_Hampshire.svg.png'},{'name':'New Jersey','flag':'9/92/Flag_of_New_Jersey.svg/45px-Flag_of_New_Jersey.svg.png'},{'name':'New Mexico','flag':'c/c3/Flag_of_New_Mexico.svg/45px-Flag_of_New_Mexico.svg.png'},{'name':'New York','flag':'1/1a/Flag_of_New_York.svg/46px-Flag_of_New_York.svg.png'},{'name':'North Carolina','flag':'b/bb/Flag_of_North_Carolina.svg/45px-Flag_of_North_Carolina.svg.png'},{'name':'North Dakota','flag':'e/ee/Flag_of_North_Dakota.svg/38px-Flag_of_North_Dakota.svg.png'},{'name':'Ohio','flag':'4/4c/Flag_of_Ohio.svg/46px-Flag_of_Ohio.svg.png'},{'name':'Oklahoma','flag':'6/6e/Flag_of_Oklahoma.svg/45px-Flag_of_Oklahoma.svg.png'},{'name':'Oregon','flag':'b/b9/Flag_of_Oregon.svg/46px-Flag_of_Oregon.svg.png'},{'name':'Pennsylvania','flag':'f/f7/Flag_of_Pennsylvania.svg/45px-Flag_of_Pennsylvania.svg.png'},{'name':'Rhode Island','flag':'f/f3/Flag_of_Rhode_Island.svg/32px-Flag_of_Rhode_Island.svg.png'},{'name':'South Carolina','flag':'6/69/Flag_of_South_Carolina.svg/45px-Flag_of_South_Carolina.svg.png'},{'name':'South Dakota','flag':'1/1a/Flag_of_South_Dakota.svg/46px-Flag_of_South_Dakota.svg.png'},{'name':'Tennessee','flag':'9/9e/Flag_of_Tennessee.svg/46px-Flag_of_Tennessee.svg.png'},{'name':'Texas','flag':'f/f7/Flag_of_Texas.svg/45px-Flag_of_Texas.svg.png'},{'name':'Utah','flag':'f/f6/Flag_of_Utah.svg/45px-Flag_of_Utah.svg.png'},{'name':'Vermont','flag':'4/49/Flag_of_Vermont.svg/46px-Flag_of_Vermont.svg.png'},{'name':'Virginia','flag':'4/47/Flag_of_Virginia.svg/44px-Flag_of_Virginia.svg.png'},{'name':'Washington','flag':'5/54/Flag_of_Washington.svg/46px-Flag_of_Washington.svg.png'},{'name':'West Virginia','flag':'2/22/Flag_of_West_Virginia.svg/46px-Flag_of_West_Virginia.svg.png'},{'name':'Wisconsin','flag':'2/22/Flag_of_Wisconsin.svg/45px-Flag_of_Wisconsin.svg.png'},{'name':'Wyoming','flag':'b/bc/Flag_of_Wyoming.svg/43px-Flag_of_Wyoming.svg.png'}];



    /** AQUI SE CARGAN LAS SERIES **/

    if (filtroSeries.tipoColeccion == 'parcial') {
      consultasbd.getSeriesOfUser(datos.getActualTableCollection()).then(
        function (dato) {
          $scope.statesWithFlags = dato.series;
        },
        function (error) {
          console.log('Error en llamada a BBDD getTemas');
        }
      );

    } else {
      consultasbd.getSeries(datos.getActualTableCollection()).then(
        function (dato) {
          $scope.statesWithFlags = dato.series;
        },
        function (error) {
          console.log('Error en llamada a BBDD getTemas');
        }
      );
    }

    $scope.$watch(
      "selected",
      function handleFooChange(newValue, oldValue) {


        console.log("vm.fooCount:", newValue);

        



          var textToSearch = $scope.selected;
          if (textToSearch.indexOf('"') != -1) {
            textToSearch = '"' + textToSearch + '"';
          }


          if (filtroSeries.tipoColeccion == 'parcial') {
            consultasbd.getColeccionSeriesOfUser(datos.getActualTableCollection(), textToSearch).then(
              function (data) {
                console.log(data);
                filtroSeries.vectorSellosMostrar = data;
               

                if (data.totalSeries > 0) { //Si hay resultados en la búsqueda se cierra automáticamente el teclado
                  console.log('Cerrar teclado');
                  cordova.plugins.Keyboard.close();
                  //datos.setLocalStorage('txtUltimaBusquedaSeries',textToSearch);
                }
              },
              function (err) {
                console.log(err);
              }
            )

          } else {
            consultasbd.getColeccionSeries(datos.getActualTableCollection(), textToSearch).then(
              function (data) {
                console.log(data);
                filtroSeries.vectorSellosMostrar = data;
                
                if (data.totalSeries > 0) { //Si hay resultados en la búsqueda se cierra automáticamente el teclado
                  console.log('Cerrar teclado');
                  cordova.plugins.Keyboard.close();
                  datos.setLocalStorage('txtUltimaBusquedaSeries', textToSearch);
                }
              },
              function (err) {
                console.log(err);
              }
            )
          }
        }
      

    );

    filtroSeries.openModalInfoSello = function (size, sello) {

      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'modulos/modales/html/modalInfoCategorias.html',
        controller: 'infoSelloController',
        size: size,
        resolve: {
          items: function () {
            return filtroSeries.vectorSellosMostrar.series[sello];
          }
        }
      });



      filtroSeries.searchByyear = function (year) {
        console.log(year);
        var nextYear = year + 1;
        var obj = {
          rango: year + " - " + nextYear,
          anios: 1,
          rangoIni: year,
          rangoFin: year + 1
        }
        // getInfoColeccionRango: function(tabla, rangoIni, rangoFin)
        consultasbd.getInfoColeccionRango(datos.getActualTableCollection(), obj.rangoIni, obj.rangoFin).then(
          function (dato) {
            var array = [];
            array.push(dato)
            datos.setVectAnio(1, array);
            var ruta = '/resultadoFiltro/' + obj.rango + "/" + obj.anios + "/0/anios/null";
            console.log(ruta);
            $scope.ok();
            ///resultadoFiltro/1925 - 1950/25/3/anios/null
            $location.path(ruta);
          },
          function (error) {
            console.log(error);
          }

        )
      };



      modalInstance.result.then(function (selectedItem) {
        //Parte en caso de ok()
        console.log('Cerrando modal');
        console.log('->' + selectedItem);
        //$scope.selected = selectedItem;
      }, function () {
        //Parte en caso de cancel()
        $log.info('Modal dismissed at: ' + new Date());
      });

    };


    $(window).scroll(function () {
      if ($(this).scrollTop() > 50) {
        $('#back-to-top').fadeIn();
      } else {
        $('#back-to-top').fadeOut();
      }
    });
    // scroll body to 0px on click

    filtroSeries.returnToTop = function (to) {
      // Prevent default anchor click behavior
      event.preventDefault();
      // Store hash
      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: 0
      }, 800, function () {
      });

    };

  }); //FIN filtroSeriesController

})();
