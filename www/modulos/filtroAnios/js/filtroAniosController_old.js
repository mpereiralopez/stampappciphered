'use strict';

(function(){
  var app = angular.module('app.filtrosAnios.controller', ['servicios']);

  app.controller('filtrosAniosController', function ($q, $scope, datos, consultasbd) {
    console.log('filtrosAniosController.js');

    var filtrosAnios = this;

    //Rango de años inicial
    filtrosAnios.radioModel = 25;
    //Se obtiene del modelo de datos la fecha inicial y final
    filtrosAnios.anios = datos.getFechas();
    //Total de años
    filtrosAnios.totalAnios = filtrosAnios.anios.fin - filtrosAnios.anios.inicio;
    //Cuantos rangos de años se obtienen
    filtrosAnios.rangoAnios = parseInt(filtrosAnios.totalAnios / filtrosAnios.radioModel);
    //Si el rango de años no es exacto, se obtienen los años restantes
    filtrosAnios.rangoAniosFin = filtrosAnios.totalAnios % filtrosAnios.radioModel;
    //Vector para iterar con el ng-repeat
    filtrosAnios.vectorAnios = [];
    filtrosAnios.vectorSellosAnio = [];

    /* Se emplea para iterar en ng-repeat */
    /*
    filtrosAnios.getRango = function(dato){
      if(filtrosAnios.rangoAniosFin === 0){
        return new Array(dato);
      }else{
        return new Array(dato+1);
      }
    };
    */

    /**
    * getRango función que genera el vector de rango de años para un ng-repeat
    * @param  {int} dato rango de años elegido por el usuario
    * @return {array} devuelve un vector que contiene los rangos de años para ser usado en el ng-repeat
    */
    filtrosAnios.getRango = function (dato){
      var rangoi;
      filtrosAnios.vectorAnios = [];
      filtrosAnios.vectorSellosAnio = [];
      if(filtrosAnios.rangoAniosFin === 0){
        for(var i = 0; dato > i; i++){
          rangoi = filtrosAnios.anios.inicio + (i*filtrosAnios.radioModel);
          filtrosAnios.vectorAnios.push(rangoi + ' - ' + (rangoi + filtrosAnios.radioModel));
          consultasbd.getInfoColeccionRango(datos.getActualTableCollection(),rangoi,(rangoi + filtrosAnios.radioModel)).then(
            function(dato){
              //console.log(dato);
              filtrosAnios.vectorSellosAnio.push(dato.length);
            },
            function(error){
              console.log(error);
            }
          );
        }
        return filtrosAnios.vectorAnios;
      }else{
        for(var i = 0; dato > i; i++){
          rangoi = filtrosAnios.anios.inicio + (i*filtrosAnios.radioModel);
          filtrosAnios.vectorAnios.push(rangoi + ' - ' + (rangoi + filtrosAnios.radioModel));
          consultasbd.getInfoColeccionRango(datos.getActualTableCollection(),rangoi,(rangoi + filtrosAnios.radioModel)).then(
            function(dato){
              //console.log(dato);
              filtrosAnios.vectorSellosAnio.push(dato.length);
            },
            function(error){
              console.log(error);
            }
          );
        }
        filtrosAnios.vectorAnios.push((rangoi + filtrosAnios.radioModel) + ' - ' + ((rangoi + filtrosAnios.radioModel) + filtrosAnios.rangoAniosFin));
        consultasbd.getInfoColeccionRango(datos.getActualTableCollection(),(rangoi + filtrosAnios.radioModel),((rangoi + filtrosAnios.radioModel) + filtrosAnios.rangoAniosFin)).then(
          function(dato){
            //console.log(dato);
            filtrosAnios.vectorSellosAnio.push(dato.length);
          },
          function(error){
            console.log(error);
          }
        );
        return filtrosAnios.vectorAnios;
      }
    };

    // filtrosAnios.getRango = function (dato){
    //     var rangoi;
    //     var campoi = {};
    //     filtrosAnios.vectorAnios = [];
    //     if(filtrosAnios.rangoAniosFin === 0){
    //       for(var i = 0; dato > i; i++){
    //         campoi = {};
    //         rangoi = filtrosAnios.anios.inicio + (i*filtrosAnios.radioModel);
    //         //filtrosAnios.vectorAnios.push(rangoi + ' - ' + (rangoi + filtrosAnios.radioModel));
    //         campoi.rangoInicial = rangoi;
    //         campoi.rangoTexto = rangoi + ' - ' + (rangoi + filtrosAnios.radioModel);
    //         filtrosAnios.vectorAnios.push(campoi);
    //       }
    //       return filtrosAnios.vectorAnios;
    //     }else{
    //       for(var i = 0; dato > i; i++){
    //         campoi = {};
    //         rangoi = filtrosAnios.anios.inicio + (i*filtrosAnios.radioModel);
    //         //filtrosAnios.vectorAnios.push(rangoi + ' - ' + (rangoi + filtrosAnios.radioModel));
    //         campoi.rangoInicial = rangoi;
    //         campoi.rangoTexto = rangoi + ' - ' + (rangoi + filtrosAnios.radioModel);
    //         filtrosAnios.vectorAnios.push(campoi);
    //       }
    //       //filtrosAnios.vectorAnios.push((rangoi + filtrosAnios.radioModel) + ' - ' + ((rangoi + filtrosAnios.radioModel) + filtrosAnios.rangoAniosFin));
    //       campoi = {};
    //       campoi.rangoInicial = rangoi+filtrosAnios.radioModel;
    //       campoi.rangoTexto = (rangoi + filtrosAnios.radioModel) + ' - ' + ((rangoi + filtrosAnios.radioModel) + filtrosAnios.rangoAniosFin);
    //       filtrosAnios.vectorAnios.push(campoi);
    //       return filtrosAnios.vectorAnios;
    //     }
    // };
    //
    // filtrosAnios.getSellosRango = function (){
    //   var promises = [];
    //   filtrosAnios.vectorSellosAnio = [];
    //
    //   angular.forEach(filtrosAnios.vectorAnios,
    //     function(key){
    //       var promise = consultasbd.getInfoColeccionRango('china_ingles',(key.rangoInicial),(key.rangoInicial + filtrosAnios.radioModel));
    //       promises.push(promise);
    //     }
    //   );
    //
    //   $q.all(promises).then(
    //     function(values){
    //       console.log(values);
    //       angular.forEach(filtrosAnios.vectorAnios,
    //         function(value, key){
    //           filtrosAnios.vectorAnios[key].totalSellos = values[key].length;
    //           console.log(key);
    //         }
    //       );
    //     },
    //     function(err){
    //       console.log(err);
    //     }
    //   );
    //
    // };

    /* Watcher sobre el botón de selección de rango de años de la interfaz de usuario */
    $scope.$watch('filtrosAnios.radioModel', function(newValue, oldValue){
      filtrosAnios.radioModel = newValue;
      filtrosAnios.rangoAnios = parseInt(filtrosAnios.totalAnios / filtrosAnios.radioModel);
      filtrosAnios.rangoAniosFin = filtrosAnios.totalAnios % filtrosAnios.radioModel;
      filtrosAnios.getRango(filtrosAnios.rangoAnios);
      // filtrosAnios.getSellosRango();
    });

  });

})();
