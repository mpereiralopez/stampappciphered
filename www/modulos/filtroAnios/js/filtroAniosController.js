'use strict';

(function(){
  var app = angular.module('app.filtrosAnios.controller', []);

  app.controller('filtrosAniosController', function ($q, $scope, datos, consultasbd, ENUMERADOS) {
    console.log('filtrosAniosController.js');

    var filtrosAnios = this;

    var tiposApp = ENUMERADOS;
    var navStatus = datos.getNavStatus();
    var radioModel = datos.getUltimoRangoAccedido();

    if(navStatus.id=='anios'){
        radioModel = parseInt(navStatus.value);
    }

    if(datos.getModo().cambioModo === true){
      //Pedir nuevo rango para el modo
      console.log('Nuevo rango de datos para el usuario !!');
    }

    //console.log(navStatus);
    //alert(radioModel);
    //Rango de años inicial
    filtrosAnios.radioModel = radioModel;
    //Se obtiene del modelo de datos la fecha inicial y final
    filtrosAnios.anios = datos.getFechas();
    //Total de años
    filtrosAnios.totalAnios = filtrosAnios.anios.fin - filtrosAnios.anios.inicio;
    //Cuantos rangos de años se obtienen
    filtrosAnios.rangoAnios = parseInt(filtrosAnios.totalAnios / filtrosAnios.radioModel);
    //Si el rango de años no es exacto, se obtienen los años restantes
    filtrosAnios.rangoAniosFin = filtrosAnios.totalAnios % filtrosAnios.radioModel;
    //Vector para iterar con el ng-repeat
    filtrosAnios.vectorAnios = [];
    filtrosAnios.vectorSellosAnio = [];

    //filtrosAnios.anioRangoMostrar = datos.getVectAnio(radioModel); //Necesario para la precarga de datos en la entrada

    filtrosAnios.tablaActual = datos.getActualTableCollection();

    filtrosAnios.sellosRango = [];

    /**
    * getRango función que genera el vector de rango de años para un ng-repeat
    * @param  {int} dato rango de años elegido por el usuario
    * @return {array} devuelve un vector que contiene los rangos de años para ser usado en el ng-repeat
    */
    filtrosAnios.getRango = function (dato){
      var rangoi;
      var contador = 0;
      var modo = datos.getModo();
      datos.showSpinner();
      filtrosAnios.vectorAnios = [];
      filtrosAnios.vectorSellosAnio = [];
      filtrosAnios.sellosRango = [];

      if(modo.modoActual === tiposApp.colecciones.completa){
          console.log("Modo catálogo");
          if(filtrosAnios.rangoAniosFin === 0){
            for(var i = 0; dato > i; i++){
              rangoi = filtrosAnios.anios.inicio + (i*filtrosAnios.radioModel);
              filtrosAnios.vectorAnios.push(rangoi + ' - ' + (rangoi + filtrosAnios.radioModel));
              consultasbd.getInfoColeccionRango(filtrosAnios.tablaActual,rangoi,(rangoi + filtrosAnios.radioModel)).then(
                function(dato){
                  //console.log(dato);
                  filtrosAnios.vectorSellosAnio.push(dato.length);
                  filtrosAnios.sellosRango.push(dato);
                  if(contador === i-1){
                    datos.setVectAnio(filtrosAnios.radioModel, filtrosAnios.sellosRango);
                    datos.setVectorSellosAnio(filtrosAnios.vectorSellosAnio);
                    datos.hideSpinner();
                  }
                  contador++;
                },
                function(error){
                  console.log(error);
                }
              );
            }
            datos.setVectorAnios(filtrosAnios.vectorAnios);
            return filtrosAnios.vectorAnios;
          }else{
            for(var i = 0; dato > i; i++){
              rangoi = filtrosAnios.anios.inicio + (i*filtrosAnios.radioModel);
              filtrosAnios.vectorAnios.push(rangoi + ' - ' + (rangoi + filtrosAnios.radioModel));
              consultasbd.getInfoColeccionRango(filtrosAnios.tablaActual,rangoi,(rangoi + filtrosAnios.radioModel)).then(
                function(dato){
                  //console.log(dato);
                  filtrosAnios.vectorSellosAnio.push(dato.length);
                  filtrosAnios.sellosRango.push(dato);
                  if(contador === i){
                    datos.setVectAnio(filtrosAnios.radioModel, filtrosAnios.sellosRango);
                    datos.setVectorSellosAnio(filtrosAnios.vectorSellosAnio);
                    datos.hideSpinner();
                  }
                  contador++;
                },
                function(error){
                  console.log(error);
                }
              );
            }
            filtrosAnios.vectorAnios.push((rangoi + filtrosAnios.radioModel) + ' - ' + ((rangoi + filtrosAnios.radioModel) + filtrosAnios.rangoAniosFin));
            consultasbd.getInfoColeccionRango(filtrosAnios.tablaActual,(rangoi + filtrosAnios.radioModel),((rangoi + filtrosAnios.radioModel) + filtrosAnios.rangoAniosFin)).then(
              function(dato){
                //console.log(dato);
                filtrosAnios.vectorSellosAnio.push(dato.length);
                filtrosAnios.sellosRango.push(dato);
                if(contador === i){
                  datos.setVectAnio(filtrosAnios.radioModel, filtrosAnios.sellosRango);
                  datos.setVectorSellosAnio(filtrosAnios.vectorSellosAnio);
                  datos.hideSpinner();
                }
                contador++;
              },
              function(error){
                console.log(error);
              }
            );
            datos.setVectorAnios(filtrosAnios.vectorAnios);
            return filtrosAnios.vectorAnios;
          }
      }else{
        console.log("Modo mis sellos");

        /****************************/
        /****************************/

        if(filtrosAnios.rangoAniosFin === 0){
            for(var i = 0; dato > i; i++){
              rangoi = filtrosAnios.anios.inicio + (i*filtrosAnios.radioModel);
              filtrosAnios.vectorAnios.push(rangoi + ' - ' + (rangoi + filtrosAnios.radioModel));
              consultasbd.getInfoUsuarioRango(filtrosAnios.tablaActual,rangoi,(rangoi + filtrosAnios.radioModel)).then(
                function(dato){
                  //console.log(dato);
                  filtrosAnios.vectorSellosAnio.push(dato.length);
                  filtrosAnios.sellosRango.push(dato);
                  if(contador === i-1){
                    datos.setVectAnio(filtrosAnios.radioModel, filtrosAnios.sellosRango);
                    datos.setVectorSellosAnio(filtrosAnios.vectorSellosAnio);
                    datos.hideSpinner();
                  }
                  contador++;
                },
                function(error){
                  console.log(error);
                }
              );
            }
            datos.setVectorAnios(filtrosAnios.vectorAnios);
            return filtrosAnios.vectorAnios;
          }else{
            for(var i = 0; dato > i; i++){
              rangoi = filtrosAnios.anios.inicio + (i*filtrosAnios.radioModel);
              filtrosAnios.vectorAnios.push(rangoi + ' - ' + (rangoi + filtrosAnios.radioModel));
              consultasbd.getInfoUsuarioRango(filtrosAnios.tablaActual,rangoi,(rangoi + filtrosAnios.radioModel)).then(
                function(dato){
                  //console.log(dato);
                  filtrosAnios.vectorSellosAnio.push(dato.length);
                  filtrosAnios.sellosRango.push(dato);
                  if(contador === i){
                    datos.setVectAnio(filtrosAnios.radioModel, filtrosAnios.sellosRango);
                    datos.setVectorSellosAnio(filtrosAnios.vectorSellosAnio);
                    datos.hideSpinner();
                  }
                  contador++;
                },
                function(error){
                  console.log(error);
                }
              );
            }
            filtrosAnios.vectorAnios.push((rangoi + filtrosAnios.radioModel) + ' - ' + ((rangoi + filtrosAnios.radioModel) + filtrosAnios.rangoAniosFin));
            consultasbd.getInfoUsuarioRango(filtrosAnios.tablaActual,(rangoi + filtrosAnios.radioModel),((rangoi + filtrosAnios.radioModel) + filtrosAnios.rangoAniosFin)).then(
              function(dato){
                //console.log(dato);
                filtrosAnios.vectorSellosAnio.push(dato.length);
                filtrosAnios.sellosRango.push(dato);
                if(contador === i){
                  datos.setVectAnio(filtrosAnios.radioModel, filtrosAnios.sellosRango);
                  datos.setVectorSellosAnio(filtrosAnios.vectorSellosAnio);
                  datos.hideSpinner();
                }
                contador++;
              },
              function(error){
                console.log(error);
              }
            );
            datos.setVectorAnios(filtrosAnios.vectorAnios);
            console.log(filtrosAnios.vectorAnios);
            return filtrosAnios.vectorAnios;
          }
























      }
    };

    /* Watcher sobre el botón de selección de rango de años de la interfaz de usuario */
    //datos.getModo().cambioModo==true controla entre mi colección y catalogo para pedir la solicitud
    $scope.$watch('filtrosAnios.radioModel', function(newValue, oldValue){
      if((newValue !== oldValue) || datos.getPrimerAcceso() || datos.getModo().cambioModo==true ){
        filtrosAnios.radioModel = newValue;
        filtrosAnios.rangoAnios = parseInt(filtrosAnios.totalAnios / filtrosAnios.radioModel);
        filtrosAnios.rangoAniosFin = filtrosAnios.totalAnios % filtrosAnios.radioModel;
        filtrosAnios.getRango(filtrosAnios.rangoAnios);
        datos.setUltimoRangoAccedido(newValue);
        datos.setPrimerAcceso();
      }else{
        filtrosAnios.sellosRango = datos.getVectAnio(newValue);
        filtrosAnios.vectorAnios = datos.getVectorAnios();
        filtrosAnios.vectorSellosAnio = datos.getVectorSellosAnio();
      }
      // filtrosAnios.getSellosRango();
      //filtrosAnios.anioRangoMostrar = datos.getVectAnio(newValue);
    });

  });

})();
