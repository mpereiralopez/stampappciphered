(function(){

  var app = angular.module('app.filtros.controller', []);

  app.controller('filtrosController', function($scope,$http,$timeout, $filter,$uibModal,basedatos,consultasbd,datos){

    var dynamicFilters = this;
    var tableHeaderName = 'headers';
    var selectLiteral = 'S';
    var lang = datos.getLocalStorage("preferredLanguage");
    if(!lang)lang="en-us";
    var condition = "ISO='"+lang+"'";
    dynamicFilters.tipoColeccion = $scope.$parent.main.tipoColeccion;

    dynamicFilters.table = datos.getActualTableCollection();

    dynamicFilters.searchLimit = 400;

    resetFilters = function(){
      dynamicFilters.showAdd = false;
    //Solo lo cargamos la primera vez
    dynamicFilters.arrayHeaders = [];
    dynamicFilters.totalSellosMostrar = 0;
    //-------------------------------
    //Array donde almaceno las condiciones que genera cada select
    dynamicFilters.conditionList = [];
    //Objeto condicion compuesto por su cabecera y su valor
    dynamicFilters.conditionObj = {id:0,disabled: false, header:{sql:'',printValue:''}, value:'',headersList: [], headersValues: []};
    //Array donde se guardan los objetos Sello para mostrar
    dynamicFilters.vectorSellosMostrar = [];

    /** FIRST FOR FILTER--> GET HERADERS **/
    consultasbd.getTranslatedHeaders(condition).then(
     function(answer) {
      //quitamos el ID
      for(var i= 0;i<answer.length;i++){
        var headerObj = {};

        if(answer[i].cid!='Name' && answer[i].cid!='Image'
          && answer[i].cid!='Description' && answer[i].cid!='Country'){
          headerObj.id = answer[i].cid;
          headerObj.name = answer[i].name;
          dynamicFilters.arrayHeaders.push(headerObj);
        }
      }
      dynamicFilters.arrayHeaders.sort(function(a,b) {
        return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0);} );
      dynamicFilters.conditionObj.headersList = dynamicFilters.arrayHeaders;
      dynamicFilters.conditionList[0] = dynamicFilters.conditionObj;
    },
    function(error) {
     console.log(error);

   },
   function(progress) {
     console.log(progress);
   });
  };



  /*********************************************************************/
    //Metodo para detectar si hay valor en los dos select para habilitar el botón de añadir
    dynamicFilters.checkIfAllHasValue = function(){
     var is1Blank = true;
     var is2Blank = true;
     $(".selected_header").each(function() {
      //console.log($(this).val());
      //console.log($(this).val().length);
      if($(this).val() !== "" && $(this).val() !== "?") {
        is1Blank = false;
      }else{
        is1Blank = true;
      }
    });
     $(".selected_value").each(function() {
      //console.log($(this).val());
      //console.log($(this).val().length);
      if($(this).val() !== "" && $(this).val() !== "?") {
        is2Blank = false;
      }else{
        is2Blank = true;
      }
    });
     dynamicFilters.showAdd = !is1Blank&&!is2Blank;

     console.log("dynamicFilters.showAdd "+dynamicFilters.showAdd);
   }


   /** Metodo que se lanza cuando se cambiar el valor del select de la izquierda **/

   dynamicFilters.update_filter_first_level = function(index, item) {
        dynamicFilters.conditionList[index].headersValues = [];

    if (!item){
      if(index!=0){
        dynamicFilters.removeFilter(index);
      }else{
        dynamicFilters.vectorSellosMostrar = [];
      }
      dynamicFilters.checkIfAllHasValue();

      return;
    }

    dynamicFilters.conditionList[index].header.printValue = item.name;
    dynamicFilters.conditionList[index].header.sql = item.id;

    var column =item.id;
    var cond = "";

    if(index > 0){
     cond +="AND " + calculateMysqlConditions(dynamicFilters.conditionList,false);
   }

   var promise = consultasbd.getFiltersSecondLevel(dynamicFilters.table,column,cond,dynamicFilters.tipoColeccion);
   promise.then(
     function(answer) {
      var objArray = {};
      for(var i= 0;i<answer.length;i++){
        var obj={}
        obj.id=i;
        obj.name = 	answer[i][column];
        objArray[i]=obj;
      }
      dynamicFilters.conditionList[index].headersValues = objArray;
      dynamicFilters.checkIfAllHasValue();
      datos.hideSpinner();
    },
    function(error) {
     console.log(error);

   },
   function(progress) {
   });
 };




 dynamicFilters.update_filter_second_level = function(index, itemSecond) {

  if(itemSecond == null){

    if(index==0){
      dynamicFilters.vectorSellosMostrar = [];
      dynamicFilters.totalSellosMostrar = 0;
    }
    dynamicFilters.checkIfAllHasValue();

    return;
  } else{
      datos.showSpinner();
  }

  dynamicFilters.conditionList[index].value = itemSecond.name;


  var promise = searchWithFilters(dynamicFilters.conditionList);

  promise.then(
   function(answer) {
     manageResults(answer);
   },
   function(error) {
    console.log("error on update_filter_second_level");
    console.log(error);
  },
  function(progress) {
    console.log(progress);
  });


};



	//Triggered when button of search is clicked

	dynamicFilters.addNewFilter=function(){
    var conditionObj = {id:dynamicFilters.conditionList.length,disabled:false,header:{sql:'',printValue:''}, value:'',headersList: [], headersValues: []};

    var aux = dynamicFilters.arrayHeaders;
    var conditionListAux = dynamicFilters.conditionList;
    var headersAccumulated = [];
    for(var i = 0; i< dynamicFilters.conditionList.length;i++){
      var obj = {};
      obj.id = dynamicFilters.conditionList[i].header.sql;
      obj.name = dynamicFilters.conditionList[i].header.printValue;
      headersAccumulated.push(obj);
      if(i != dynamicFilters.conditionList.length){
        dynamicFilters.conditionList[i].disabled = true;
      }
    }

    var finalArray = aux.concat(headersAccumulated);
    var returnedArray = [];
    var result = aux.filter(function(item1) {
      for (var i in headersAccumulated) {
        if (item1.id === headersAccumulated[i].id) { return false; }
      };
      return true;
    });

      conditionObj.headersList = result;
      dynamicFilters.conditionList.push(conditionObj);

      console.log(dynamicFilters.conditionList);

      $timeout(function () {
        //$scope.loading = false;
        dynamicFilters.checkIfAllHasValue()
      }, 100);
    }



    dynamicFilters.removeFilter=function(index){

      if(index == 0){
        resetFilters();
        return;
      }
      datos.showSpinner();
      dynamicFilters.conditionList.splice(index, 1);

      for(var i= 0; i<dynamicFilters.conditionList.length;i++){
        if(i != dynamicFilters.conditionList.length-1){
          dynamicFilters.conditionList[i].disabled = true;
        }else{
          dynamicFilters.conditionList[i].disabled = false;
        }
      }



      var promise = searchWithFilters(dynamicFilters.conditionList);

      promise.then(
       function(answer) {
        manageResults(answer);
      },
      function(error) {
       console.log(error);

     },
     function(progress) {
       console.log(progress);
     });
    }


    dynamicFilters.goToResults=function(){

      var resultsArray=[];

      for(var i=0; i<dynamicFilters.resultadosFinal.length;i++){
        resultsArray.push(dynamicFilters.resultadosFinal.item(i));
      }

      datos.addFilteredSello(resultsArray);
      $scope.$parent.main.go('/resultadoFiltro',  $scope.$parent.main.tipoColeccion, [0,0, 0] );
    }


    dynamicFilters.openModalInfoSello = function (size, sello) {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'modulos/modales/html/modalInfoCategorias.html',
        controller: 'infoSelloController',
        size: size,
        resolve: {
          items: function () {
            return dynamicFilters.vectorSellosMostrar[sello];
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
    //Parte en caso de ok()
    //$scope.selected = selectedItem;
  },function () {
    //Parte en caso de cancel()
    $log.info('Modal dismissed at: ' + new Date());
  });
    }

    dynamicFilters.addSello = function(indice){
      dynamicFilters.vectorSellosMostrar[indice].cantidad++;
      consultasbd.setColeccionUsuario(dynamicFilters.vectorSellosMostrar[indice].Id, dynamicFilters.vectorSellosMostrar[indice].cantidad);

    };

    dynamicFilters.remSello = function(indice){
      dynamicFilters.vectorSellosMostrar[indice].cantidad--;
      if(dynamicFilters.vectorSellosMostrar[indice].cantidad <= 0){
        dynamicFilters.vectorSellosMostrar[indice].cantidad = 0;
        consultasbd.setColeccionUsuario(dynamicFilters.vectorSellosMostrar.temas[indice].Id, dynamicFilters.vectorSellosMostrar[indice].cantidad);
      }
    };


    manageResults = function (answer){
      dynamicFilters.totalSellosMostrar = answer.rows.length;
      if(answer.rows.length>=dynamicFilters.searchLimit){
        dynamicFilters.vectorSellosMostrar = [];
        //Su busqueda produjo muchos resultados. Indique mas filtros de busqueda
         var textoToShow = $filter('translate')('FILTERS_TOO_MUCH');
          alert(textoToShow);
      }else{
        var arrayAux = [];
        for(var i= 0;i<answer.rows.length;i++){
         if (answer.rows.item(i).cantidad==null)answer.rows.item(i).cantidad = 0;
         arrayAux.push(answer.rows.item(i));

       }
       dynamicFilters.vectorSellosMostrar = arrayAux;
     }

   //dynamicFilters.totalSellos = dynamicFilters.vectorSellosMostrar.length;
    dynamicFilters.checkIfAllHasValue();
   datos.hideSpinner();
 }



	//Triggered when any second filter is selected

	searchWithFilters=function(conditionList){
		var createConditions = calculateMysqlConditions(conditionList,true);
		var all = ["*"];

    if(dynamicFilters.tipoColeccion == 'parcial'){
      createConditions+= ' AND coleccion_usuario.cantidad > 0 ';
      console.log("partialCreate Conditions "+createConditions);
    }

    createConditions+= " LIMIT "+dynamicFilters.searchLimit;

    console.log(createConditions);

    return basedatos.callBd("J", [["*"], ['cantidad']],  [dynamicFilters.table, "coleccion_usuario"], {type:["L"], join:[["Id"],["id_sello"]], where: createConditions});



		//return basedatos.callBd('S', all, datos.getActualTableCollection(), createConditions);
	};

  calculateMysqlConditions = function (conditionList,isTotal){
    var createConditions = "";
    var limit = 0;
    if(isTotal) limit = conditionList.length;
    else limit = conditionList.length -1;

    var str;
    for(var i=0; i<limit;i++){
      if(typeof conditionList[i].value === 'string' && conditionList[i].value.indexOf('"')!=-1){
        str = '"'+conditionList[i].value+'"';
      }else{
        str = conditionList[i].value;
      }
      createConditions+=conditionList[i].header.sql+'="'+str+'"';
      if(i!= limit-1)createConditions+= " AND ";
    }

    console.log(createConditions);
    return createConditions;
  };

  $(window).scroll(function () {
      if ($(this).scrollTop() > 50) {
        $('#back-to-top').fadeIn();
      } else {
        $('#back-to-top').fadeOut();
      }
    });
    // scroll body to 0px on click

    dynamicFilters.returnToTop = function (to) {
      // Prevent default anchor click behavior
      event.preventDefault();
      // Store hash
      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: 0
      }, 800, function () {
      });

    };

  resetFilters();

});

})();
