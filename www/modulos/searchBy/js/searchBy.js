'use strict';

(function(){
  var app = angular.module('app.searchby.controller', []);

  app.controller('searchbyController', function ($log, $scope, $q, $filter, $uibModal, datos, consultasbd) {
    console.log('searchbyController.js');

    var searchby = this;

    searchby.textoFormulario = $filter('translate')('TXTFORMTEMA_SEARCHBY');
    //console.log($translate('TXTFORMTEMA'));
    searchby.disableSearch = true;
    searchby.vectorSellosMostrar = [];
    searchby.vectorSellosMostrar.totalSellos = 0;

    searchby.buscarSellosPorTexto = function(){
      var value = $scope.selected;
      
      if(value!= undefined && value.trim().length>=3){
              datos.showSpinner();

        var promisedResults = consultasbd.getColeccionNombre(datos.getActualTableCollection(),value.trim());
        promisedResults.then(
          function(answer) {
            console.log(answer);
                //alert(answer.length);
                searchby.vectorSellosMostrar = answer;
                searchby.vectorSellosMostrar.totalSellos = answer.length;
                datos.hideSpinner();
                datos.setLocalStorage('txtUltimaBusquedaFree',$scope.selected);
              },
              function(error) {
                console.log(error);
                datos.hideSpinner();
              },
              function(progress) {
                console.log(progress);
              });
      }else{
        alert("Error");
      }
    };

    searchby.limpiarSellosText = function(){
      datos.showSpinner();
      $scope.selected = '';
      datos.setLocalStorage('txtUltimaBusquedaFree','');
      searchby.vectorSellosMostrar = [];
      searchby.vectorSellosMostrar.totalSellos = 0;
      datos.hideSpinner();
    };

    searchby.ocultarSoftkeys = function(){
      datos.ocultarSoftkeys();
    };

    searchby.addSello = function(indice){
      searchby.vectorSellosMostrar[indice].cantidad++;
      consultasbd.setColeccionUsuario(searchby.vectorSellosMostrar[indice].Id, searchby.vectorSellosMostrar[indice].cantidad);
  
    };

    searchby.remSello = function(indice){
      searchby.vectorSellosMostrar[indice].cantidad--;
      if(searchby.vectorSellosMostrar[indice].cantidad <= 0){
        searchby.vectorSellosMostrar[indice].cantidad = 0;
        consultasbd.setColeccionUsuario(searchby.vectorSellosMostrar[indice].Id, searchby.vectorSellosMostrar[indice].cantidad);
      }
    };

    $scope.selected = datos.getLocalStorage('txtUltimaBusquedaFree');


    $scope.$watch(
      "selected",
      function handleFooChange( newValue, oldValue ) {
        console.log( "vm.fooCount:", newValue );
      // filtroCategorias.buscarSellosCategoria();
      console.log("AQUI");
      if(($scope.selected === undefined) || ($scope.selected.length < 3)){
        console.log("deactivate");
        searchby.disableSearch = true;
      }else{
        searchby.disableSearch = false;
      }
    }
    );

    searchby.openModalInfoSello = function (size, sello) {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'modulos/modales/html/modalInfoCategorias.html',
        controller: 'infoSelloController',
        size: size,
        resolve: {
          items: function () {
            return searchby.vectorSellosMostrar[sello];
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
    //Parte en caso de ok()
    console.log('Cerrando modal');
    console.log('->' + selectedItem);
    //$scope.selected = selectedItem;
  },function () {
    //Parte en caso de cancel()
    $log.info('Modal dismissed at: ' + new Date());
  });

    };

    $(window).scroll(function () {
      if ($(this).scrollTop() > 50) {
        $('#back-to-top').fadeIn();
      } else {
        $('#back-to-top').fadeOut();
      }
    });
    // scroll body to 0px on click

    searchby.returnToTop = function (to) {
      // Prevent default anchor click behavior
      event.preventDefault();
      // Store hash
      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: 0
      }, 800, function () {
      });

    };

  }); //FIN filtroCategoriasController

})();
