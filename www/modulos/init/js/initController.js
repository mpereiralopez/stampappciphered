'use strict';

(function(){
var app = angular.module('app.init.controller', []);

app.controller('initController', function ($scope, $q, $location, $timeout, datos, ENUMERADOS, consultasbd) {
  console.log('initController.js');
  //alert();
  var mainInit = this;
  // var vectAnios1 = new Array(Math.ceil((datos.getFechas().fin - datos.getFechas().inicio) / 1));
  // var vectAnios5 = new Array(Math.ceil((datos.getFechas().fin - datos.getFechas().inicio) / 5));
  // var vectAnios10 = new Array(Math.ceil((datos.getFechas().fin - datos.getFechas().inicio) / 10));
  // var vectAnios25 = new Array(Math.ceil((datos.getFechas().fin - datos.getFechas().inicio) / 25));
  // var vectAnios50 = new Array(Math.ceil((datos.getFechas().fin - datos.getFechas().inicio) / 50));
  // var vectAnios75 = new Array(Math.ceil((datos.getFechas().fin - datos.getFechas().inicio) / 75));


  mainInit.spinnerCarga = false;
  //mainInit.visibilidadCabecera = datos.getVisibilidadCabecera();
  // var arrayTextos = {1: 'vectAnios1', 5: 'vectAnios5', 10: 'vectAnios10', 25: 'vectAnios25', 50: 'vectAnios50', 75: 'vectAnios75'};

  //alert();

  //Para rescatar los idiomas que tenemos en la BBDD dependiendo de las tablas
  var blackList =['sqlite_sequence','datos_usuario','coleccion_usuario','headers','android_metadata'];
  mainInit.defaultTableForCollection = '';

  var tablesInDataBase = consultasbd.getTablesOfDataBase();
  mainInit.langAvailables = [];
  tablesInDataBase.then(
     function(answer) {
      var collectionCountry = '';
      for(var i = 1; i < answer.length; i++){
        if(blackList.indexOf(answer[i].name) === -1){
          collectionCountry = answer[i].name.split('_')[0];
          mainInit.langAvailables.push(answer[i].name.split('_')[1]);
          mainInit.defaultTableForCollection = answer[i].name;
        }

      }
      datos.setActualTableCollection(mainInit.defaultTableForCollection);
      datos.setCollectionCountry(collectionCountry);
      datos.setAvailableLanguages(mainInit.langAvailables);
      $scope.$parent.main.banderaColeccion = datos.getBanderaColeccionApp();

      /** RESCATAMOS LOS SELLOS DE LA COLECCION Y DEL USUARIO DE LA BBDD */

      var sellosEnColeccion = consultasbd.getTotalColeccion(mainInit.defaultTableForCollection);
      sellosEnColeccion.then(
          function(answer){
            console.log(answer);
            $scope.$parent.main.sellosColeccion = answer.totalColeccion;
          }
        );
        consultasbd.getTotalUsuarioDebug();

       var sellosDeUsuario = consultasbd.getTotalUsuario();
      sellosDeUsuario.then(
          function(answer){
            console.log(answer);
            $scope.$parent.main.sellosUsuario = answer.totalUsuario;
          }
        );

           /** ***************************************** */


      // getSellosRango(75);
      // getSellosRango(50);
      // getSellosRango(25);
      // getSellosRango(10);
      // getSellosRango(5);
      // getSellosRango(1);
      getSellosRango(0);

    },
    function(error) {
     console.log(error);

   },
   function(progress) {
     console.log(progress);
   });

  mainInit.cont = 0;

  function getSellosRango(paso){
    // var promises = [];

    console.log('PANTALLA OFF');
    mainInit.spinnerCarga = true;

    // for (var i = 0; i < vectAnios1.length; (i = i+paso)) {
    //   var promise = consultasbd.getInfoColeccionRango(mainInit.defaultTableForCollection,1850+i,1850+i+paso);
    //   //console.log(1850+i);
    //   //console.log(1850+(i+paso));
    //   promises.push(promise);
    // }

    // $q.all(promises).then(
    //   function(values){
    //     console.log(values);
    //     datos.setVectAnio(paso, values);
        //angular.forEach(obj, function(value, key){

        //});
        // console.log('BBDD FIN -> ' + paso);
        // mainInit.cont++;
        // console.log("-----------------------------");
        // console.log(mainInit.cont + " / 6" );
        // console.log("-----------------------------");
        // if(mainInit.cont >= 6){
          console.log('PANTALLA ON');
          $timeout(function () {
            mainInit.spinnerCarga = false;
            $scope.$parent.main.visibilidadCabecera = true;
            $location.path(ENUMERADOS.rutas.home);
          }, 1500);
    //     }
    //   },
    //   function(err){
    //     console.log(err);
    //   }
    // );

  };

});

})();
