'use strict';

(function () {
  var app = angular.module('app.modales.controller', []);

  /* Se encarga de mostrar toda la información de un sello seleccionado */
  app.controller('infoSelloController', function ($scope, $location, $uibModalInstance, $translate, items, consultasbd, datos, $filter) {
    console.log('infoSelloController');
    console.log(items);

    consultasbd.getInfoSello(datos.getActualTableCollection(), items.Id).then(
      function (dato) {
        console.log(dato);
        $scope.items = dato;
      },
      function (error) {
        console.log(error);
      }
    );
    console.log($scope.items);

    //$scope.items = items;
    // $scope.selected = {
    //   item: $scope.items[0]
    // };

    $scope.ok = function () {
      console.log('ok pulsado');
      //$uibModalInstance.close($scope.selected.item);
      $uibModalInstance.close('dato desde modal');
    };

    $scope.cancel = function () {
      console.log('cancel pulsado');
      $uibModalInstance.dismiss('cancel');
    };


    $scope.searchByyear = function (year) {
      console.log(year);
      var nextYear = year + 1;
      var obj = {
        rango: year + ' - ' + nextYear,
        anios: 1,
        rangoIni: year,
        rangoFin: year + 1
      };
      // getInfoColeccionRango: function(tabla, rangoIni, rangoFin)
      consultasbd.getInfoColeccionRango(datos.getActualTableCollection(), obj.rangoIni, obj.rangoFin).then(
        function (dato) {
          var array = [];
          array.push(dato);
          datos.setVectAnio(1, array);
          var ruta = '/resultadoFiltro/' + obj.rango + '/' + obj.anios + '/0/anios/null';
          console.log(ruta);
          $scope.ok();
          ///resultadoFiltro/1925 - 1950/25/3/anios/null
          $location.path(ruta);
        },
        function (error) {
          console.log(error);
        }

      );
    };

    $scope.searchByTheme = function (themeValue) {
      if (themeValue !== undefined) {
        datos.setLocalStorage('txtUltimaBusqueda', themeValue);
        $scope.ok();
        $location.path('/filtroCategorias');
      }
    };

    //SOCIAL SHARING

    $scope.shareWithSocial = function (item, socialNet) {
      console.log(item)
      console.log(socialNet);

      var plat=device.platform;
      var  message=$filter('translate')('MESSAGESOCIAL')+items.Country+$filter('translate')('MESSAGESOCIALEND');
      var urlmes="http://gloeasy.com"
      var choosermes=$filter('translate')('MESSAGESOCIALCHOOSER');
      var choosersubject=$filter('translate')('MESSAGESOCIALSUBJECT');


      if(plat=="Android"){
          message=$filter('translate')('MESSAGESOCIALANDROID')+items.Country+$filter('translate')('MESSAGESOCIALANDROIDEND');
          var baseURL = "https://play.google.com";
          urlmes = baseURL+"/store/apps/developer?id=Gloeasy";
      }
      if(plat=="iOS"){
          message=$filter('translate')('MESSAGESOCIALIOS')+items.Country+$filter('translate')('MESSAGESOCIALIOSEND');
          //urlmes = "https://itunes.apple.com/us/developer/apple/id284417353?mt=12";
            urlmes = "https://itunes.apple.com/";
      }


      var options = {
        message: message+' '+items.Name, // not supported on some apps (Facebook, Instagram)
        subject: choosersubject, // fi. for email
        files: ['data:image/png;base64,'+items.ImageB64], // an array of filenames either locally or remotely
        url: urlmes,
        chooserTitle: choosermes // Android only, you can override the default share sheet title
      }

      var onSuccess = function (result) {
        console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
        console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
      }

      var onError = function (msg) {
        console.log("Sharing failed with message: " + msg);
        alert("This option is not available in your smartphone, try other");
      }



      switch (socialNet) {
        case 0:
          //Twitter
          window.plugins.socialsharing.shareViaTwitter(options.message, options.files[0] /* img */, options.url);

          break;

        case 1:
          //FB
          window.plugins.socialsharing.shareViaFacebook(options.message, options.files[0] /* img */, options.url, onSuccess, onError);
          break;

        case 2:
          //Instagram
          window.plugins.socialsharing.shareViaInstagram(options.message, options.files[0] /* img */, onSuccess, onError);
          break;

        case 3:
          //Email
          window.plugins.socialsharing.shareViaEmail(
            options.message, // can contain HTML tags, but support on Android is rather limited:  http://stackoverflow.com/questions/15136480/how-to-send-html-content-with-image-through-android-default-email-client
            options.subject,
            null, // TO: must be null or an array
            null, // CC: must be null or an array
            null, // BCC: must be null or an array
             options.files[0], // FILES: can be null, a string, or an array
            onSuccess, // called when sharing worked, but also when the user cancelled sharing via email. On iOS, the callbacks' boolean result parameter is true when sharing worked, false if cancelled. On Android, this parameter is always true so it can't be used). See section "Notes about the successCallback" below.
            onError // called when sh*t hits the fan
          );
          break;

        default:
          //Email
          window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
          break;
      }


    };




  });//END infoSelloController


  /* Se encarga de lanzar la selección de idiomas */
  app.controller('selectorIdiomaController', function ($scope, $uibModalInstance, $translate, datos, items) {
    console.log('selectorIdiomaController');
    console.log('Idioma: ' + items);

    $scope.langsAvailableObjArray = [];
    var langs = datos.getAvailableLanguages();

    for (var i = 0; i < langs.length; i++) {
      var obj = {};
      obj.flagIcon = datos.getBanderaOfLanguage(langs[i]);
      obj.text = datos.getTextOfLanguage(langs[i]);
      obj.id = datos.getIdOfLanguage(langs[i]);
      $scope.langsAvailableObjArray.push(obj);
    }

    $scope.changeLanguage = function (langId) {

      var actualDDBB = datos.getActualTableCollection();

      var base = actualDDBB.substr(0, actualDDBB.indexOf('_') + 1);

      var newDDBB = base + langId;
      datos.setActualTableCollection(newDDBB);
      datos.setActualLang(langId);

      var dict = datos.getDictionaryById(langId);
      console.log(dict);

      var jsonFile = dict.jsonLang;
      console.log($scope.$parent);
      console.log($scope.selected);
      items = dict;
      console.log(items);
      $translate.use(jsonFile);
      $scope.ok();

    };

    $scope.ok = function () {
      console.log('ok pulsado');
      //$uibModalInstance.close($scope.selected.item);
      console.log(items);
      $uibModalInstance.close(items);

    };

    $scope.cancel = function () {
      console.log('cancel pulsado');
      $uibModalInstance.dismiss('cancel');
    };
  });//END selectorIdiomaController

})();
