(function() {

    var app = angular.module('app.news.controller', []);

    app.controller('newsController', function($scope, $sce, $twitter, datos) {


        var news = this;

        news.isOnline = true;

        isOnline(
            function() {
                //alert("Sorry, we currently do not have Internet access.");
                news.isOnline = false;
            },
            function() {
                //alert("Succesfully connected!");
                news.isOnline = true;
            }
        );


        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        };

        $scope.ArrayOfTwitts = [];

        var lang = datos.getActualLang();
        //alert(lang);
        var hashTag = datos.getHashTagOfForLanguageCode(lang);

        $twitter.login(function(response) {
            $twitter.searchByHashtag(function(response) {
                console.log(response);
                if (response.statuses) {
                    response = response.statuses;
                }
                console.log(response);
                for (var i = 0; i < response.length; i++) {
                    var obj = response[i];
                    if (typeof obj === 'string') {
                        obj = JSON.parse(response[i]);
                    }
                    //obj.user.profile_image_url = $sce.trustAsResourceUrl(obj.user.profile_image_url);
                    obj.created_at = obj.created_at.slice(0, 3) + '-' + obj.created_at.slice(4, 7) + '-' + obj.created_at.slice(8, 11) + ' (' + obj.created_at.slice(26, 30) + ')';
                    var tweetText = obj.text;
                    obj.text = linkify(tweetText)
                    $scope.ArrayOfTwitts.push(obj);
                }
            }, function(err) {
                console.log("Error on search ");
                console.log(err);

            }, hashTag);
        }, function(error) {
            console.log("error on plugin");
            console.log(error);
        });

        $(window).scroll(function() {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click

        news.returnToTop = function(to) {
            // Prevent default anchor click behavior
            event.preventDefault();
            // Store hash
            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: 0
            }, 800, function() {});

        };



    });


    function linkify(tweetText) {
        var urlPattern = /\b(?:https?|ftp):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|]/gim;
        var pseudoUrlPattern = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
        var emailAddressPattern = /[\w.]+@[a-zA-Z_-]+?(?:\.[a-zA-Z]{2,6})+/gim;

        var replacedText = tweetText.replace(urlPattern, '<a href="$&">$&</a>')
        replacedText = replacedText.replace(pseudoUrlPattern, '$1<a href="http://$2">$2</a>')
        replacedText = replacedText.replace(emailAddressPattern, '<a href="mailto:$&">$&</a>');
        return replacedText;
    }



    function success(msg) {
        console.log(msg);
    }

    function err(err) {
        console.log(err);
    }

    function isOnline(no, yes) {
        var xhr = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHttp');
        xhr.onload = function() {
            if (yes instanceof Function) {
                yes();
            }
        }
        xhr.onerror = function() {
            if (no instanceof Function) {
                no();
            }
        }
        xhr.open("GET", "http://google.es", true);
        xhr.send();
    }



})();