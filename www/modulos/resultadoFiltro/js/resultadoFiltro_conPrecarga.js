'use strict';

(function(){
var app = angular.module('app.resultadoFiltro.controller', []);

  app.controller('resultadoFiltroController', function ($log, $scope, $routeParams, $uibModal, datos, consultasbd) {
    console.log('resultadoFiltroController');
    console.log('Rango: ' + $routeParams.rango + 'Años: ' + $routeParams.anios + 'Posición: ' + $routeParams.posicion);
    console.log($routeParams);
    var resultadoFiltro = this;
    //Rango: 0Años: 0Posición: 0



    resultadoFiltro.anios = $routeParams.rango;
    //console.log()

    resultadoFiltro.addSello = function(indice){
      resultadoFiltro.vectorSellosMostrar[indice].Coleccion++;
      consultasbd.setColeccionUsuario(resultadoFiltro.vectorSellosMostrar[indice].Id, resultadoFiltro.vectorSellosMostrar[indice].Coleccion);
    };

    resultadoFiltro.remSello = function(indice){
      resultadoFiltro.vectorSellosMostrar[indice].Coleccion--;
      if(resultadoFiltro.vectorSellosMostrar[indice].Coleccion <= 0){
        resultadoFiltro.vectorSellosMostrar[indice].Coleccion = 0;
        consultasbd.setColeccionUsuario(resultadoFiltro.vectorSellosMostrar[indice].Id, resultadoFiltro.vectorSellosMostrar[indice].Coleccion);
      }
    };

    var posicion = 5;
    var pasoPosicion = 5;


    if($routeParams.rango==0&&$routeParams.anios==0&&$routeParams.posicion==0){
        var sellos = datos.getFilteredSello();
        console.log(sellos);
        datos.setNavStatus($routeParams.type,$routeParams.text);
        resultadoFiltro.textSearch = $routeParams.text;
        resultadoFiltro.vectorSellosMostrarAux = sellos;
    }else{

        datos.setNavStatus('anios',$routeParams.anios);
        var anio = parseInt($routeParams.anios);
        var pos = parseInt($routeParams.posicion);
        var result = datos.getVectAnio(anio);
        resultadoFiltro.vectorSellosMostrarAux = datos.getVectAnio(anio)[pos];
    }

    resultadoFiltro.vectorSellosMostrar = resultadoFiltro.vectorSellosMostrarAux.slice(0,pasoPosicion);

    resultadoFiltro.infiniteScrollFuncion = function(){
      console.log("SCROLL !!");
      console.log(posicion + ' - ' + (posicion + pasoPosicion));
      if(resultadoFiltro.vectorSellosMostrarAux.length > posicion){
        for (var i = posicion; i < (posicion + pasoPosicion); i++) {
          resultadoFiltro.vectorSellosMostrar[i] = resultadoFiltro.vectorSellosMostrarAux[i];
        }
        posicion = posicion + pasoPosicion;
      }
    };

    resultadoFiltro.openModalInfoSello = function (size, sello) {

      var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'modulos/modales/html/modalInfoSello.html',
      controller: 'infoSelloController',
      size: size,
      resolve: {
        items: function () {
          return resultadoFiltro.vectorSellosMostrar[sello];
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      //Parte en caso de ok()
      console.log('Cerrando modal');
      console.log('->' + selectedItem);
      $scope.selected = selectedItem;
    },function () {
      //Parte en caso de cancel()
      $log.info('Modal dismissed at: ' + new Date());
    });

    };

  });//FIN resultadoFiltroController

})();
