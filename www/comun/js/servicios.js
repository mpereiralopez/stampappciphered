(function (){
  var app = angular.module('servicios', []);

  app.factory('datos', function(){

    var datosApp = {};

    var localStorageVars = [{'nombre': 'txtUltimaBusqueda'}, {'nombre': 'txtUltimaBusquedaSeries'},{'nombre':'LANG_TEXT'},{'nombre':'preferredLanguage'}];

    //@logoUser permite mostrar == true, ocultar == false el icono de usuario de la home
    //@logoFlecha permite mostrar == true, ocultar == false el icono de la flecha para volver atras
    datosApp.controlCabecera = {logoUser: true, logoFlecha: false};

    //Guarda la información sobre el modo de acceso, toda la colección == 'catalogo', colección del usuario == 'misSellos'
    datosApp.modo = {modoActual: '', cambioModo: false};

    //Spinner general de la aplicación
    // spinner.visibilidad == true mostrar, false == ocultar
    // spinner.texto == texto a mostrar por pantalla
    datosApp.spinner = {visibilidad: false, texto: '', textos: ['CARGANDODATOS']};

    //Indica la visibilidad de la cabecera, inicialmente, mientras se realiza la carga de datos se oculta.
    //homeController se encarga de ponerla a true
    datosApp.visibilidadCabecera = false;

    //inicio almacena el año del primer sello de la coleccion, fin almacena el año del último sello de la coleccion
    //datosApp.fechas = {inicio: -1, fin: -1};
    datosApp.fechas = {inicio: 1850, fin: 2017};

    //Controla si se ha realizado un acceso a la app
    datosApp.primerAcceso = true;

    //Vectores que contienen toda la información respecto del rango de años seleccionado
    datosApp.vectAnios1 = [];
    datosApp.vectAnios5 = [];
    datosApp.vectAnios10 = [];
    datosApp.vectAnios25 = [];
    datosApp.vectAnios50 = [];
    datosApp.vectAnios75 = [];

    //Vector que contiene los rango de años para el rango seleccionado
    datosApp.vectorAnios = [];

    //Vector que contiene los sellos para cada rango de año según el rango seleccionado
    datosApp.vectorSellosAnio = [];

    datosApp.ultimoRangoAccedido = 25;
    //Here is setted stamps searched by filters no years.
    datosApp.vectorFilters = [];

    datosApp.navStatus ={
      id:"",
      value:""
    };

    datosApp.availableLanguages = [];

    datosApp.actualLang = "en-us";

    datosApp.collectionCountry = '';

    datosApp.actualTableCollection = '';

    datosApp.langsFlagDictionary = [
    {
      id:"EN",
      country:"gran_bretana",
      flag:"flag-icon-us",
      text:"English",
      hashtag:"Philately+OR+stamps+OR+philatelic+OR+stamps+OR+aerofilatelia+OR+postmark",
      jsonLang :"en-us",
      jsonLangEntry:"IDIOMAS.ENG"
    },
    {
      id:"CN",
      country:"china",
      flag:"flag-icon-cn",
      text:"中国",
      hashtag:"集郵+OR+郵票+OR+集郵+OR+郵票+OR+郵戳",
      jsonLang :"zh-cn",
      jsonLangEntry:"IDIOMAS.CHN"
    }
    ];

    /*******************/

    /* GETTERS y SETTERS */
    return {
      getPrimerAcceso: function(){
        return datosApp.primerAcceso;
      },

      setPrimerAcceso: function(){
        datosApp.primerAcceso = false;
      },

      getModo: function(){
        return datosApp.modo;
      },

      setModo: function(modo, cambio){
        if((modo === 'completa') || (modo === 'parcial') || (modo === 'ninguno')){
          datosApp.modo.modoActual = modo;
          datosApp.modo.cambioModo = cambio;
        }
      },

      getUltimoRangoAccedido: function(){
        return datosApp.ultimoRangoAccedido;
      },

      setUltimoRangoAccedido:function(rango){
        datosApp.ultimoRangoAccedido = rango;
      },

      getDictionaryById : function(id){
        var dict;
        var dictionariesArray = datosApp.langsFlagDictionary;
        for(var i =0; i<dictionariesArray.length;i++){
          if(dictionariesArray[i].id == id) dict = dictionariesArray[i]
        }
      return  dict
    },

    getJsonLangById : function(id){
      var jsonLang = '';
      var dictionary = datosApp.langsFlagDictionary;
      for(var i =0; i<dictionary.length;i++){
        if(dictionary[i].id == id) jsonLang = dictionary[i].jsonLang;
      }
      return  jsonLang
    },

    getActualTableCollection:function(){
      return datosApp.actualTableCollection;
    },

    getIdOfLanguage:function(lang){
     var idResult = '';
     var dictionary = datosApp.langsFlagDictionary;
     for(var i =0; i<dictionary.length;i++){
      if(dictionary[i].id == lang) idResult = dictionary[i].id;
    }
    return  idResult;
  },

  getHashTagOfForLanguageCode:function(ISO){
    var hashtag =  datosApp.langsFlagDictionary[0].hashtag;

    for(var i=0; i<datosApp.langsFlagDictionary.length;i++){
      if(datosApp.langsFlagDictionary[i].id==ISO)hashtag=datosApp.langsFlagDictionary[i].hashtag;
    }

    return hashtag;
  },


  setActualTableCollection:function(tableCollection){
    datosApp.actualTableCollection = tableCollection;
  },

  getBanderaColeccionApp:function(){
    var collectionCountry = datosApp.collectionCountry;
    var flagResult = '';
    var dictionary = datosApp.langsFlagDictionary;
    for(var i =0; i<dictionary.length;i++){
      if(dictionary[i].country == collectionCountry) flagResult = dictionary[i].flag;
    }
    return  flagResult;
  },

  getBanderaOfLanguage:function(lang){
    var flagResult = '';
    var dictionary = datosApp.langsFlagDictionary;
    for(var i =0; i<dictionary.length;i++){
      if(dictionary[i].id == lang) flagResult = dictionary[i].flag;
    }
    return  flagResult;
  },

  getTextOfLanguage:function(lang){
    var textResult = '';
    var dictionary = datosApp.langsFlagDictionary;
    for(var i =0; i<dictionary.length;i++){
      if(dictionary[i].id == lang) textResult = dictionary[i].text;
    }
    return  textResult;
  },

  getAvailableLanguages:function(){
   return datosApp.availableLanguages;
 },

 setAvailableLanguages:function(langs){
  datosApp.availableLanguages = langs;
},

getCollectionCountry:function(){
 return datosApp.collectionCountry;
},

setCollectionCountry:function(country){
  datosApp.collectionCountry = country;
},


getActualLang:function(){
  return datosApp.actualLang;
},

setActualLang:function(lang){
  datosApp.actualLang=lang;
},


getNavStatus:function(){
  return datosApp.navStatus;
},

setNavStatus:function(id,value){
  datosApp.navStatus.id=id;
  datosApp.navStatus.value=value;
},


getCabecera: function(){
  return datosApp.controlCabecera;
},
setCabecera: function(userBool, flechaBool){
  datosApp.controlCabecera.logoUser = userBool;
  datosApp.controlCabecera.logoFlecha = flechaBool;
},

/* VISIBILIDAD HEADER DE LA APP */
getVisibilidadCabecera: function(){
  return datosApp.visibilidadCabecera;
},
setVisibilidadCabecera: function(visibilidad){
  datosApp.visibilidadCabecera = visibilidad;
},

showSpinner: function(){
  angular.element(document.querySelector('html')).addClass('noScrollModal');
  angular.element(document.querySelector('body')).addClass('noScrollModal');
  datosApp.spinner.visibilidad = true;
        //datosApp.spinner.texto = datosApp.spinner.textos[id];
        // firing an event upwards
      },

      hideSpinner: function(){
        angular.element(document.querySelector('html')).removeClass('noScrollModal');
        angular.element(document.querySelector('body')).removeClass('noScrollModal');
        datosApp.spinner.visibilidad = false;
      },

      getSpinner: function(){
        return datosApp.spinner.visibilidad;
      },

      /* FECHAS PARA BÚSQUEDA EN CATÁLOGO Y COLECCIÓN DEL USUARIO */
      getFechas: function(){
        return datosApp.fechas;
      },
      setFechas: function(inicio, fin){
        datosApp.fechas.inicio = inicio;
        datosApp.fechas.fin = fin;
      },

      /* VECTORES CON LOS SELLOS POR AÑOS */
      getVectAnio: function(anio){
        switch (anio) {
          case 1:
          return datosApp.vectAnios1;
          break;
          case 5:
          return datosApp.vectAnios5;
          break;
          case 10:
          return datosApp.vectAnios10;
          break;
          case 25:
          return datosApp.vectAnios25;
          break;
          case 50:
          return datosApp.vectAnios50;
          break;
          case 75:
          return datosApp.vectAnios75;
          break;
          default:
          console.log('Error al recuperar los vectores de años');
        }
      },
      setVectAnio: function(anio, vectorDatos){
        switch (anio) {
          case 1:
          datosApp.vectAnios1 = vectorDatos;
          break;
          case 5:
          datosApp.vectAnios5 = vectorDatos;
          break;
          case 10:
          datosApp.vectAnios10 = vectorDatos;
          break;
          case 25:
          datosApp.vectAnios25 = vectorDatos;
          break;
          case 50:
          datosApp.vectAnios50 = vectorDatos;
          break;
          case 75:
          datosApp.vectAnios75 = vectorDatos;
          break;
          default:
          console.log('Error al guardar los vectores de años');
        }
      },

      getVectorAnios: function(){
        return datosApp.vectorAnios;
      },

      setVectorAnios: function(vectAnios){
        datosApp.vectorAnios = vectAnios;
      },

      getVectorSellosAnio: function(){
        return datosApp.vectorSellosAnio;
      },

      setVectorSellosAnio: function(vectSellosAnios){
        datosApp.vectorSellosAnio = vectSellosAnios;
      },

      addFilteredSello : function(sellos) {
        datosApp.vectorFilters = sellos;
      },

      getFilteredSello : function(){
        return datosApp.vectorFilters;
      },

      ocultarSoftkeys : function(){
        AndroidFullScreen.immersiveMode(
          function(){
            console.log('Teclado + SoftKeys ocultadas');
          },
          function(error){
            console.log('Error al ocultar el teclado + softkeys');
          }
          );
      },

      getLocalStorage: function(clave){
        for (var i = 0; i < localStorageVars.length; i++){
          if (localStorageVars[i].nombre == clave){
            return localStorage.getItem(clave);
          }
        }
      },

      setLocalStorage: function(clave, valor){
        for (var i = 0; i < localStorageVars.length; i++){
          if (localStorageVars[i].nombre == clave){
            localStorage.setItem(clave, valor);
            return 1;
          }
        }
      }

    };

  });//END datos

app.factory('ENUMERADOS', function(){

  var tiposEnumerados = {
    rutas: {
      principal: '/',
      home: '/home',
      sellos: '/sellos',
      filtroAnios: '/filtroAnios',
      filtroCategorias: "/filtroCategorias",
      filtroSeries: "/filtroSeries",
      filtros: '/filtros',
      resultadoFiltro:'/resultadoFiltro',
      noticias:'/news',
      searchby:'/searchby',
      shopcart:'/shopcart',
      shop: '/shop',
      poi: '/poi'
    },
    colecciones: {
      completa: 'completa', //factory 'datos' usa (setModo) un literal que se tiene que llamar igual que este
      usuario: 'usuario' //factory 'datos' usa (setModo) un literal que se tiene que llamar igual que este
    }
  };

  return tiposEnumerados;

  });//END ENUMERADOS

app.factory('modales', function(){

  return {modalData: true};

  });//END ENUMERADOS











































































































































































































































































































































































































































































































































































































































































































































































































































































































































































  /**
   * Llamada general para base de datos
   * @param   {string}          type        Tipo de acción a realizar
   * @param   {object/array}    fields      Campos o campos y valores o array de campos y valores
   * @param   {string}          table       Nombre de la tabla para la acción
   * @param   {string}          condition   Condición de la acción
   * @return  {object}                      Objeto el resultado de las acciones realizadas
   */

   /*
   * ********** BORRAR TABLA **********
   * type         {string}    "P"                     Tipo de acción a realizar (P -> Drop)
   * fields       {null}      ""                      No utilizado
   * table        {string}    "prueba"                Nombre de la tabla a borrar
   * condition    {null}      ""                      No utilizado
   *
   * ********** CREAR TABLA **********
   * type         {string}    "C"                     Tipo de acción a realizar (C -> Create)
   * fields       {object}    {JSON}                  Objeto con los nombres de campo y, por cada uno de ellos, tipo, longitud (opcional), escala (opcional), nulo y clave primaria
   *                                                  Estructura:   {campo: {type:{string}, length:{integer}, scale:{integer}, pk:{integer (1|0)}, ai:{integer (1|0)}, notnull:{integer (1|0)}, fk:{string}}}
   *                                                  Ejemplo:      {id: {type:"integer", length:0, scale:0, pk:1, ai:1, notnull:1, fk:""}, nombre: {type:"text", length:50, scale:0, pk:0, ai:0, notnull:1, fk:""}}
   *                                                  Nota 1: los campos con auto incremento (ai:1) serán obligatoriamente de tipo "integer", "not null" y con "length" y "scale" 0
   *                                                  Nota 2: en el caso de existir más de un campo con auto incremento solo se tomará en cuenta el primero de ellos, los demás serán "unique not null"
   *                                                  Nota 3: en el caso de existir un campo con auto incremento y otros como clave primaria, estos últimos serán definidos como "unique nor null"
   * table        {string}    "prueba"                Nombre de la tabla a crear
   * condition    {null}      ""                      No utilizado
   *
   * ********** BORRAR REGISTROS **********
   * type         {string}    "D"                     Tipo de acción a realizar (D -> Delete)
   * fields       {null}      ""                      No utilizado
   * table        {string}    "prueba"                Nombre de la tabla a borrar
   * condition    {string}    "id=1 AND nombre='A'"   Condición completa sin la palabra "WHERE" (opcional)
   *
   * ********** INSERTAR REGISTROS **********
   * type         {string}    "I"                     Tipo de acción a realizar (I -> Insert)
   * fields       {object}    {id: 1, nombre: "A"}    Datos a insertar estableciendo el nombre del campo como clave y el valor como valor de dicha clave
   * table        {string}    "prueba"                Nombre de la tabla a borrar
   * condition    {null}      ""                      No utilizado
   *
   * ********** INSERTAR MULTIPLES REGISTROS **********
   * type         {string}    "M"                     Tipo de acción a realizar (M -> Insert)
   * fields       {array}     []                      Datos a insertar estableciendo el nombre del campo como clave y el valor como valor de dicha clave
   *                                                  Estructura:   [{campo1: valor1, campo2: valor2}, {campo3: valor3, campo3: valor4}]
   *                                                  Ejemplo:      [{id: 2, nombre: "B"}, {id: 3, nombre: "C"}]
   * table        {string}    "prueba"                Nombre de la tabla a borrar
   * condition    {null}      ""                      No utilizad
   *
   * ********** INSERTAR O ACTUALIZAR REGISTROS **********
   * type         {string}    "R"                     Tipo de acción a realizar (R -> Insert or Replace)
   * fields       {object}    {id: 1, nombre: "A"}    Datos a insertar o actualizar estableciendo el nombre del campo como clave y el valor como valor de dicha clave
   * table        {string}    "prueba"                Nombre de la tabla a borrar
   * condition    {null}      ""                      No utilizado
   *
   * ********** SELECCIONAR REGISTROS **********
   * type         {string}    "S"                     Tipo de acción a realizar (S -> Select)
   * fields       {array}     ["*"] | ["nombre"]      Datos a seleccionar o * en en caso de que sean todos
   * table        {string}    "prueba"                Nombre de la tabla en la que seleccionar
   * condition    {string}    "id=1"                  Condición completa sin la palabra "WHERE" (opcional)
   *
   * ********** ACTUALIZAR REGISTROS **********
   * type         {string}    "U"                     Tipo de acción a realizar (U -> Update)
   * fields       {object}    {nombre: "D"}           Datos a actualizar estableciendo el nombre del campo como clave y el valor como valor de dicha clave
   * table        {string}    "prueba"                Nombre de la tabla en la que actualizar
   * condition    {string}    "id=1"                  Condición completa sin la palabra "WHERE" (opcional)
   *
   * ********** SELECCIONAR SQLITE_MASTER **********
   * type         {string}    "Q"                     Tipo de acción a realizar (Q -> Sqlite_master)
   * fields       {null}      ""                      No utilizado
   * table        {string}    "prueba"                Nombre de la tabla de la que recoger la información
   * condition    {null}      ""                      No utilizado
   *
   * ********** SELECCIONAR REGISTROS DE VARIAS TABLAS **********
   * type         {string}    "J"                     Tipo de acción a realizar (J -> Join)
   * fields       {array}     ejemplo abajo           Datos a seleccionar de cada tabla o * en cada tabla en caso de que sean todos
   * table        {array}     ejemplo abajo           Nombre de las tablas en las que seleccionar
   * condition    {object}    ejemplo abajo           Tipo de "join", array de campos de cada tabla por los que hacer el "join" y condición completa sin la palabra "WHERE" (opcional)
   * EJEMPLO fields = [[f1, f2, f3], [f4, f5, f6], [f7, f8, f9]]
   * EJEMPLO table = [t1, t2, t3]
   * EJEMPLO condition = {type:["I","L","R"], join:[["f1", "f2"],["f4", "f5"],["f7"]], where: "t1.f1=1"}
   */

   app.factory('basedatos', function ($q){

    var db = {}; // Puntero a BBDD

    return {
      callBd: function(type, fields, table, condition){
        //console.log("Entrando...");
        var batch = false;
        var sql = "";
        var dato = [];
        var error = false;
        // Creamos la promise
        var defferer = $q.defer();

        // Apertura o creación (si no existe) de la BD
        if (Object.keys(db).length == 0){
        db =  window.sqlitePlugin.openDatabase({name: "stampapp.db", key:'prueba', location: 0}, function () {/*console.log("OK abierta");*/}, function () {/*console.log("KO abierta");*/});
      }

      if (Object.keys(db).length != 0){

        switch (type) {

          case 'P':
          if(typeof table == "string"){
            sql += "DROP TABLE IF EXISTS " + table;
            batch = false;
          } else {
            error = true;
          }
          break;

          case 'C':
          if (fields == ""){fields = bd[table]};

          if(typeof table == "string" && fields instanceof Object && !(fields instanceof Array)){
            var campos = "";
            var pks = "";
            var fks = "";
            var ais = 0;
            var ain = false;

            angular.forEach(fields, function(val, key) {
              if (val['ai'] != undefined && typeof val['ai'] == "number" && val['ai'] != "" && val['ai'] != " " && val['ai'] != 0){
                ais += 1;
                ain = true;
              }
            });

            angular.forEach(fields, function(val, key) {
              campos += key;

              if (ain){
                campos += " integer PRIMARY KEY AUTOINCREMENT NOT NULL";
                ain = false;
              } else {

                if(val['type'] != undefined && typeof val['type'] == "string" && val['type'] != "" && val['type'] != " "){

                  switch (val['type']) {
                    case "timestamp":
                    campos += " integer";
                    break;

                    case "string":
                    campos += " text";
                    break;

                    case "decimal":
                    campos += " numeric";
                    break;

                    default:
                    campos += " "+val['type'];
                    break;
                  }

                  if(val['length'] != undefined && typeof val['length'] == "number" && val['length'] != "" && val['length'] != " " && val['length'] != 0){
                    campos += " (" + parseInt(val['length']);
                    if(val['scale'] != undefined && typeof val['scale'] == "number" && val['scale'] != "" && val['scale'] != " " && val['scale'] != 0){
                      campos += "," + parseInt(val['scale']);
                    }
                    campos += ")";
                  }

                }

                if ((ais >= 1 && val['ai'] != undefined && typeof val['ai'] == "number" && val['ai'] != "" && val['ai'] != " " && val['ai'] != 0) ||
                  (ais >= 1 && val['pk'] != undefined && typeof val['pk'] == "number" && val['pk'] != "" && val['pk'] != " " && val['pk'] != 0)){
                  campos += " UNIQUE NOT NULL";
              } else if (val['notnull'] != undefined && typeof val['notnull'] == "number" && val['notnull'] != "" && val['notnull'] != " " && val['notnull'] != 0){
                campos += " NOT NULL";
              }

            }

            if (ais == 0 && val['pk'] != undefined && typeof val['pk'] == "number" && val['pk'] != "" && val['pk'] != " " && val['pk'] != 0){
              pks += ""+key+", ";
            }

            if (val['fk'] != undefined && typeof val['fk'] == "string" && val['fk'] != "" && val['fk'] != " "){
              fks += " FOREIGN KEY ("+key+") REFERENCES "+val['fk']+"("+key+") ON UPDATE CASCADE ON DELETE CASCADE, ";
            }

            campos += ", ";

          });

            campos = campos.substr(0, campos.length-2);

            if (pks != "" && fks != ""){
              pks = pks.substr(0, pks.length-2);
              fks = fks.substr(0, fks.length-2);
              sql += "CREATE TABLE IF NOT EXISTS " + table +" ("+campos+", PRIMARY KEY ("+pks+"), "+fks+")";
            } else if (pks != ""){
              pks = pks.substr(0, pks.length-2);
              sql += "CREATE TABLE IF NOT EXISTS " + table +" ("+campos+", PRIMARY KEY ("+pks+"))";
            } else if (fks != ""){
              fks = fks.substr(0, fks.length-2);
              sql += "CREATE TABLE IF NOT EXISTS " + table +" ("+campos+", "+fks+")";
            } else {
              sql += "CREATE TABLE IF NOT EXISTS " + table +" ("+campos+")";
            }

            batch = false;

          } else {
            error = true;
          }
          break;

          case 'D':
          if(typeof table == "string"){

            sql += "DELETE FROM " + table;

            if(typeof condition == "string" && condition != "" && condition != " "){
              sql += " WHERE " + condition;
            }

            batch = false;

          } else {
            error = true;
          }
          break;

          case 'I':
          if(typeof table == "string" && fields instanceof Object && !(fields instanceof Array)){
            var campo = "";
            var valor = "";

            sql += "INSERT INTO " + table;

            angular.forEach(fields, function(val, key) {
              campo += key+', ';
              valor += '?, ';
              dato.push(val);
            });

            campo = campo.substr(0, campo.length-2);
            valor = valor.substr(0, valor.length-2);

            sql += " ("+campo+") VALUES ("+valor+")";
            batch = false;

          } else {
            error = true;
          }
          break;

          case 'M':
          if(typeof table == "string" && fields instanceof Array){
            sql = [];

            angular.forEach(fields, function(val, key) {
              var sent = "";
              var campo = "";
              var valor = "";
              var data = [];

              sent += "INSERT INTO " + table;

              angular.forEach(val, function(val2, key2) {
                campo += key2+', ';
                valor += '?, ';
                data.push(val2);
              });

              campo = campo.substr(0, campo.length-2);
              valor = valor.substr(0, valor.length-2);

              sent += " ("+campo+") VALUES ("+valor+")";

              sql.push([sent, data]);
              dato.push(data);

              batch = true;

            });

          } else {
            error = true;
          }
          break;

          case 'R':
          if(typeof table == "string" && fields instanceof Object && !(fields instanceof Array)){
            var campo = "";
            var valor = "";

            sql += "INSERT OR REPLACE INTO " + table;

            angular.forEach(fields, function(val, key) {
              campo += key+', ';
              valor += '?, ';
              dato.push(val);
            });

            campo = campo.substr(0, campo.length-2);
            valor = valor.substr(0, valor.length-2);

            sql += " ("+campo+") VALUES ("+valor+")";
            batch = false;

          } else {
            error = true;
          }
          break;

          case 'S':
          if(typeof table == "string" && fields instanceof Array){

            sql += "SELECT ";

            angular.forEach(fields, function(val, key) {
              sql += val+', ';
            });

            sql = sql.substr(0, sql.length-2);

            sql += " FROM " + table;

            if(typeof condition == "string" && condition != "" && condition != " "){
              sql += " WHERE " + condition;
            }

            batch = false;

          } else {
            error = true;
          }
          break;

          case 'U':
          if(typeof table == "string" && fields instanceof Object && !(fields instanceof Array)){

            sql += "UPDATE " + table + " SET ";

            angular.forEach(fields, function(val, key) {
              sql += key + "='" + val + "', ";
            });

            sql = sql.substr(0, sql.length-2);

            if(typeof condition == "string" && condition != "" && condition != " "){
              sql += " WHERE " + condition;
            }

            batch = false;

          } else {
            error = true;
          }
          break;

          case 'Q':
          if(typeof table == "string"){

            sql += "SELECT sql FROM sqlite_master WHERE name='" + table+ "' AND type='table'";

            batch = false;

          } else {
            error = true;
          }
          break;

          case 'J':

          if(table instanceof Array && fields instanceof Array && condition instanceof Object && !(condition instanceof Array) &&
            table.length > 1 && fields.length > 0 && condition.join.length > 1 && condition.type.length > 0){
                // Faltaría comparar: (table.length == condition.join.length == condition.type.length-1)

              sql += "SELECT ";

              for (var i=0; i<table.length; i++){

                for (var j=0; j<fields[i].length; j++){
                  if (fields[i][j].substr(0,5).toLowerCase() == "count"){
                    sql += fields[i][j] + ", ";
                  } else {
                    sql += table[i] + "." + fields[i][j] + ", ";
                  }
                }

              }

              sql = sql.substr(0, sql.length-2);
              sql += " FROM " + table[0];

              for (var i=1; i<table.length; i++){
                switch (condition.type[i-1]) {

                  case 'I':
                  sql += " INNER ";
                  break;

                  case 'L':
                  sql += " LEFT ";
                  break;

                  case 'R':
                  sql += " RIGHT ";
                  break;

                  default:
                  sql += " INNER ";
                  break;
                }

                sql += "JOIN " + table[i] + " ON ";

                for (var j=0; j<condition.join[i].length; j++){
                  sql += table[i-1] + "." + condition.join[i-1][j] + "=" + table[i] + "." + condition.join[i][j] + " AND ";
                }

                sql = sql.substr(0, sql.length-5);

              }

              if(typeof condition.where == "string" && condition.where != "" && condition.where != " "){
                sql += " WHERE " + condition.where;
              }
              console.log("SQL HERE");

              console.log(sql);

              batch = false;

            } else {
              error = true;
            }
            break;

            case 'PR':
            sql += "PRAGMA table_info('"+table+"');"
            break;

            case 'INFO':
            sql += "SELECT name FROM sqlite_master WHERE type='table'";
            break;

            default:
            error = true;
          }

          if (batch && !error) {

            if(sql instanceof Array){

              db.sqlBatch(sql,
                function() {
                //console.log("OK BACTH");
                defferer.resolve(1);
              }, function(error) {
                //console.log("KO BACTH");
                defferer.reject(error);
              });

            } else {
              defferer.reject("red");
            }

          } else if (!error) {
            if(typeof sql == "string" && dato instanceof Array){

              db.executeSql(sql, dato,
                function (res) {
                //console.log("OK SQL");
                defferer.resolve(res);
              }, function(error) {
                //console.log("KO SQL");
                defferer.reject(error);
              });

              // Forma anterior de hacer la ejecución SQL
              /*
              db.transaction(function(tx) {
                tx.executeSql(sql, dato,
                function(tx, res) {
                  //console.log("OK TX");
                  defferer.resolve(res);
                }, function(error) {
                  //console.log("KO TX");
                  defferer.reject(error);
                });
              }, function(error) {
                //console.log("KO SQL");
                defferer.reject(error);
              }, function() {
                //console.log("OK SQL");
                defferer.resolve(res);
              });
              */
            } else {
              defferer.reject("red");
            }

          } else {
            defferer.reject("red");
          }

          //console.log("SQL: " + JSON.stringify(sql));
          //console.log("DAT: " + JSON.stringify(dato));

          // Cierre de la BD
          //db.close(function () {/*console.log("OK cerrada");*/}, function () {/*console.log("KO cerrada");*/});

        } else {
          defferer.reject("red");
        }

        return defferer.promise;
      }

    };

  });



app.factory('consultasbd', ['$q', 'basedatos', function($q, basedatos){

  return {
      getTotalColeccion: function(tabla){ // A1
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("S", ["COUNT (Id)"], tabla, "").then(
          function(res){
            //console.log(res);
            dato.totalColeccion = res.rows.item(0)["COUNT (Id)"];
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },

      getTotalUsuarioDebug: function(){ // A2 -- ACTUALIZAR DOCUMENTO
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("S", ["*"], 'coleccion_usuario', "").then(
          function(res){


            for(var i = 0 ;i<res.rows.length;i++ ){
              console.log(res.rows.item(i));
            }



          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },

      getTotalUsuario: function(){ // A2 -- ACTUALIZAR DOCUMENTO
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("S", ["COUNT (Id)"], 'coleccion_usuario', "cantidad > 0").then(
          function(res){
            //console.log(res);
            dato.totalUsuario = res.rows.item(0)["COUNT (Id)"];
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getInfoSello: function(tabla, idSello){ // A3
        var defferer = $q.defer();

        basedatos.callBd("S", ["*"], tabla, "Id = " + idSello).then(
          function(res){
            //console.log(res);
            defferer.resolve(res.rows.item(0));
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getTotalColeccionRango: function(tabla, rangoIni, rangoFin){ // B1
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("S", ["COUNT (Id)"], tabla, "Year BETWEEN " + rangoIni + " AND " + rangoFin).then(
          function(res){
            //console.log(res);
            dato.totalColeccionRango = res.rows.item(0)["COUNT (Id)"];
            dato.rango = rangoIni + ' - ' + rangoFin;
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getTotalUsuarioRango: function(tabla, rangoIni, rangoFin){ // B2
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("J", [["COUNT ("+tabla+".Id)"], []], [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: tabla + ".Year >= " + rangoIni + " AND " + tabla + ".Year <= " + rangoFin + " AND coleccion_usuario.cantidad > 0"}).then(
          function(res){
            //console.log(res);
            dato.totalUsuarioRango = res.rows.item(0)["COUNT ("+tabla+".Id)"];
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getInfoColeccionRango: function(tabla, rangoIni, rangoFin){ // B3
        var defferer = $q.defer();
        var dato = [];
        var valor;
        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], ['cantidad']],  [tabla, "coleccion_usuario"], {type:["L"], join:[["Id"],["id_sello"]], where: "Year BETWEEN " + rangoIni + " AND " + rangoFin + " ORDER BY Year"}).then(

//        basedatos.callBd("S", ["Id", "Name", "Year", "ImageB64", "Coleccion"], tabla, "Year BETWEEN " + rangoIni + " AND " + rangoFin + " ORDER BY Year").then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              valor = res.rows.item(i);
              if(rangoFin <= 2016){
                valor['rango'] = rangoIni + ' - ' + rangoFin;
              }else{
                valor['rango'] = rangoIni + ' - 2016';
              }

              if(valor.cantidad === null)valor.cantidad = 0;

              dato.push(valor);
              //dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getInfoUsuarioRango: function(tabla, rangoIni, rangoFin){ // B4
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], ['cantidad']], [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: tabla + ".Year BETWEEN " + rangoIni + " AND " + rangoFin +  " AND coleccion_usuario.cantidad > 0 ORDER BY YEAR"}).then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              if(res.rows.item(i).cantidad === null)res.rows.item(i).cantidad = 0;
              dato.push(res.rows.item(i));

            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getTemas: function(tabla){ // C1
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("S", ["Themes"], tabla, "Themes != 'null' GROUP BY Themes ORDER BY Themes ASC").then(
          function(res){
            //console.log(res);
            dato.totalTemas = res.rows.length;
            dato.temas = [];

            for (var i=0; i<res.rows.length; i++){
              dato.temas.push(res.rows.item(i).Themes);
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },

      getTemasOfUser: function(tabla){ // C1
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("J", [["Themes"], []],  [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: " "+tabla+".Themes != '' GROUP BY "+tabla+".Themes ORDER BY "+tabla+".Themes ASC"}).then(
        //basedatos.callBd("S", ["Themes"], tabla, "Themes != 'null' AND Coleccion > 0 GROUP BY Themes ORDER BY Themes ASC").then(
        function(res){
            //console.log(res);
            dato.totalTemas = res.rows.length;
            dato.temas = [];

            for (var i=0; i<res.rows.length; i++){
              dato.temas.push(res.rows.item(i).Themes);
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getColeccionTemas: function(tabla, tema){ // C2
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], ['cantidad']],  [tabla, "coleccion_usuario"], {type:["L"], join:[["Id"],["id_sello"]], where: 'lower(' + tabla + '.Themes) = "' + tema.toLowerCase() + '"'}).then(
        //basedatos.callBd("S", ["Coleccion, Id, ImageB64, Name, Year"], tabla, "Themes = '" + tema + "' ORDER BY Year").then(
        function(res){
          dato.totalTemas = res.rows.length;
          dato.temas = [];
          for (var i=0; i < res.rows.length; i++){
            if(res.rows.item(i).cantidad === null)res.rows.item(i).cantidad = 0;
            dato.temas.push(res.rows.item(i));
          }
          defferer.resolve(dato);
        },
        function(error){
          console.log(error);
        }
        );
        return defferer.promise;
      },
       getColeccionTemasOfUser: function(tabla, tema){ // C2
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], ['cantidad']],  [tabla, "coleccion_usuario"], {type:["L"], join:[["Id"],["id_sello"]], where: "lower(" + tabla + ".Themes) = '" + tema.toLowerCase() + "' AND coleccion_usuario.cantidad > 0"}).then(
        //basedatos.callBd("S", ["Coleccion, Id, ImageB64, Name, Year"], tabla, "Themes = '" + tema + "' ORDER BY Year").then(
        function(res){
          dato.totalTemas = res.rows.length;
          dato.temas = [];
          for (var i=0; i < res.rows.length; i++){
              //console.log(res.rows.item(i));
              dato.temas.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            console.log(error);
          }
          );
        return defferer.promise;
      },

      getUsuarioTemas: function(tabla, tema){ // C3
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("J", [["COUNT ("+tabla+".Id)"], []], [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: "lower(" + tabla + ".Themes) = '" + tema.toLowerCase() + "' AND coleccion_usuario.cantidad > 0"}).then(
          function(res){
            //console.log(res);
            dato.totalUsuarioTemas = res.rows.item(0)["COUNT ("+tabla+".Id)"];
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getInfoColeccionTemas: function(tabla, tema){ // C4
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("S", ["Id", "Name", "Year", "ImageB64"], tabla, "lower(Themes) = '" + tema.toLowerCase() + "'").then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getInfoUsuarioTemas: function(tabla, tema){ // C5
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], []],  [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: "lower(" + tabla + ".Themes) = '" + tema.toLowerCase() + "' AND coleccion_usuario.cantidad > 0"}).then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getSeries: function(tabla){ // D1
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("S", ["Series"], tabla, "Series != 'null' GROUP BY Series ORDER BY Series ASC").then(
          function(res){
            //console.log(res);
            dato.totalSeries = res.rows.length;
            dato.series = [];

            for (var i=0; i<res.rows.length; i++){
              dato.series.push(res.rows.item(i).Series);
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },

      getSeriesOfUser: function(tabla){ // D1
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("J", [["Series"], []],  [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: " "+tabla+".Series != '' GROUP BY "+tabla+".Series ORDER BY "+tabla+".Series ASC"}).then(
          function(res){
            console.log(res);
            dato.totalSeries = res.rows.length;
            dato.series = [];

            for (var i=0; i<res.rows.length; i++){
              dato.series.push(res.rows.item(i).Series);
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },



      getColeccionSeries: function(tabla, serie){ // D2
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], ['cantidad']],  [tabla, "coleccion_usuario"], {type:["L"], join:[["Id"],["id_sello"]], where: 'lower(' + tabla + '.Series) = "' + serie.toLowerCase() + '"'}).then(

        //basedatos.callBd("S", ["Coleccion, Id, ImageB64, Name, Year"], tabla, "Series = '" + serie + "' ORDER BY Year").then(
        function(res){
          dato.totalSeries = res.rows.length;
          dato.series = [];
          for (var i=0; i < res.rows.length; i++){
              //console.log(res.rows.item(i));
              if(res.rows.item(i).cantidad === null)res.rows.item(i).cantidad = 0;
              dato.series.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            console.log(error);
          }
          );

        return defferer.promise;
      },

       getColeccionSeriesOfUser: function(tabla, serie){ // D2
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], ['cantidad']],  [tabla, "coleccion_usuario"], {type:["L"], join:[["Id"],["id_sello"]], where: "lower(" + tabla + ".Series) = '" + serie.toLowerCase() + "'  AND coleccion_usuario.cantidad > 0"}).then(

          function(res){
            dato.totalSeries = res.rows.length;
            dato.series = [];
            for (var i=0; i < res.rows.length; i++){
              //console.log(res.rows.item(i));
              dato.series.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            console.log(error);
          }
          );

        // basedatos.callBd("S", ["COUNT (Id)"], tabla, "lower(Series) = '"+ serie.toLowerCase() + "'").then(
        //   function(res){
        //     //console.log(res);
        //     dato.totalColeccionSeries = res.rows.item(0)["COUNT (Id)"];
        //     defferer.resolve(dato);
        //   },
        //   function(error){
        //     //console.log(error);
        //     defferer.reject(error);
        //   }
        // );

        return defferer.promise;
      },

      getInfoColeccionSeries: function(tabla, serie){ // D4
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("S", ["Id", "Name", "Year", "ImageB64"], tabla, "lower(Series) = '" + serie.toLowerCase() + "'").then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },

      getColeccionNombre: function(tabla, nombre){ // E1
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], ['cantidad']],  [tabla, "coleccion_usuario"], {type:["L"], join:[["Id"],["id_sello"]], where: "lower(Name) LIKE '%" + nombre.toLowerCase() + "%'"}).then(
        //basedatos.callBd("S", ["Coleccion, Id, ImageB64, Name, Year"], tabla, "lower(Name) LIKE '%" + nombre.toLowerCase() + "%'").then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              if(res.rows.item(i).cantidad === null)res.rows.item(i).cantidad = 0;
               dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getUsuarioNombre: function(tabla, nombre){ // E2
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], []], [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: "lower(" + tabla + ".Name) LIKE '%" + nombre.toLowerCase() + "%' AND coleccion_usuario.cantidad > 0"}).then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getFechaEmisiones: function(tabla){ // E3
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("S", ["Year"], tabla, "Year != 'null' GROUP BY Year ORDER BY Year ASC").then(
          function(res){
            //console.log(res);
            dato.totalFechaEmisiones = res.rows.length;
            dato.fechaEmisiones = [];

            for (var i=0; i<res.rows.length; i++){
              dato.fechaEmisiones.push(res.rows.item(i).Year);
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getColeccionFechaEmision: function(tabla, fecha){ // E4
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("S", ["Id", "Name", "Year", "ImageB64"], tabla, "Year = " + fecha).then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getUsuarioFechaEmision: function(tabla, fecha){ // E5
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], []], [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: tabla + ".Year = " + fecha + " AND coleccion_usuario.cantidad > 0"}).then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getEmisiones: function(tabla){ // E6
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("S", ["Emission"], tabla, "Emission != 'null' GROUP BY Emission ORDER BY Emission ASC").then(
          function(res){
            //console.log(res);
            dato.totalEmisiones = res.rows.length;
            dato.emisiones = [];

            for (var i=0; i<res.rows.length; i++){
              dato.emisiones.push(res.rows.item(i).Emission);
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getColeccionEmision: function(tabla, emision){ // E7
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("S", ["Id", "Name", "Year", "ImageB64"], tabla, "lower(Emission) = '" + emision.toLowerCase() + "'").then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getUsuarioEmision: function(tabla, emision){ // E8
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], []], [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: "lower(" + tabla + ".Emission) = '" + emision.toLowerCase() + "' AND coleccion_usuario.cantidad > 0"}).then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getImpresiones: function(tabla){ // E9
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("S", ["Printing"], tabla, "Printing != 'null' GROUP BY Printing ORDER BY Printing ASC").then(
          function(res){
            //console.log(res);
            dato.totalImpresiones = res.rows.length;
            dato.impresiones = [];

            for (var i=0; i<res.rows.length; i++){
              dato.impresiones.push(res.rows.item(i).Printing);
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getColeccionImpresion: function(tabla, impresion){ // E10
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("S", ["Id", "Name", "Year", "ImageB64"], tabla, "lower(Printing) = '" + impresion.toLowerCase() + "'").then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getUsuarioImpresion: function(tabla, impresion){ // E11
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], []], [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: "lower(" + tabla + ".Printing) = '" + impresion.toLowerCase() + "' AND coleccion_usuario.cantidad > 0"}).then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getPuntuaciones: function(tabla){ // E12
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("S", ["Score"], tabla, "Score != 'null' GROUP BY Score ORDER BY Score ASC").then(
          function(res){
            //console.log(res);
            dato.totalPuntuaciones = res.rows.length;
            dato.puntuaciones = [];

            for (var i=0; i<res.rows.length; i++){
              dato.puntuaciones.push(res.rows.item(i).Score);
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getColeccionPuntuacion: function(tabla, puntuacion){ // E13
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("S", ["Id", "Name", "Year", "ImageB64"], tabla, "Score = " + puntuacion).then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getUsuarioPuntuacion: function(tabla, puntuacion){ // E14
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], []], [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: tabla + ".Score = " + puntuacion + " AND coleccion_usuario.cantidad > 0"}).then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getPrecisiones: function(tabla){ // E15
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("S", ["Accuracy"], tabla, "Accuracy != 'null' GROUP BY Accuracy ORDER BY Accuracy ASC").then(
          function(res){
            //console.log(res);
            dato.totalPrecisiones = res.rows.length;
            dato.precisiones = [];

            for (var i=0; i<res.rows.length; i++){
              dato.precisiones.push(res.rows.item(i).Accuracy);
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getColeccionPrecision: function(tabla, precision){ // E16
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("S", ["Id", "Name", "Year", "ImageB64"], tabla, "lower(Accuracy) = '" + precision.toLowerCase() + "'").then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getUsuarioPrecision: function(tabla, precision){ // E17
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], []], [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: "lower(" + tabla + ".Accuracy) = '" + precision.toLowerCase() + "' AND coleccion_usuario.cantidad > 0"}).then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getColores: function(tabla){ // E18
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("S", ["Colors"], tabla, "Colors != 'null' GROUP BY Colors ORDER BY Colors ASC").then(
          function(res){
            //console.log(res);
            dato.totalColores = res.rows.length;
            dato.colores = [];

            for (var i=0; i<res.rows.length; i++){
              dato.colores.push(res.rows.item(i).Colors);
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getColeccionColores: function(tabla, color){ // E19
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("S", ["Id", "Name", "Year", "ImageB64"], tabla, "lower(Colors) = '" + color.toLowerCase() + "'").then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getUsuarioColores: function(tabla, color){ // E20
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], []], [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: "lower(" + tabla + ".Colors) = '" + color.toLowerCase() + "' AND coleccion_usuario.cantidad > 0"}).then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getPerforaciones: function(tabla){ // E24
        var defferer = $q.defer();
        var dato = {};

        basedatos.callBd("S", ["Perforation"], tabla, "Perforation != 'null' GROUP BY Perforation ORDER BY Perforation ASC").then(
          function(res){
            //console.log(res);
            dato.totalPerforaciones = res.rows.length;
            dato.perforaciones = [];

            for (var i=0; i<res.rows.length; i++){
              dato.perforaciones.push(res.rows.item(i).Perforation);
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getColeccionPerforacion: function(tabla, perforacion){ // E25
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("S", ["Id", "Name", "Year", "ImageB64"], tabla, "lower(Perforation) = '" + perforacion.toLowerCase() + "'").then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getUsuarioPerforacion: function(tabla, perforacion){ // E26
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("J", [["Id", "Name", "Year", "ImageB64"], []], [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: "lower(" + tabla + ".Perforation) = '" + perforacion.toLowerCase() + "' AND coleccion_usuario.cantidad > 0"}).then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getPrimerAno: function(tabla){
        var defferer = $q.defer();
        var dato = 0;

        basedatos.callBd("S", ["Year"], tabla, "Year != 'null' GROUP BY Year ORDER BY Year ASC LIMIT 0,1").then(
          function(res){
            //console.log(res);
            dato = parseInt(res.rows.item(0).Year);

            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getUltimoAno: function(tabla){
        var defferer = $q.defer();
        var dato = 0;

        basedatos.callBd("S", ["Year"], tabla, "Year != 'null' GROUP BY Year ORDER BY Year DESC LIMIT 0,1").then(
          function(res){
            //console.log(res);
            dato = parseInt(res.rows.item(0).Year);

            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      getPaises: function(tabla){
        var defferer = $q.defer();
        var dato = [];

        basedatos.callBd("S", ["Country"], tabla, "Country != 'null' GROUP BY Country ORDER BY Country ASC").then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }

            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },

      getFiltersSecondLevel:function(tabla,column,conditions, tipoColeccion){
        var defferer = $q.defer();
        var dato = [];
        var cadena = column+" != 'null'"+conditions+" GROUP BY "+column+" ORDER BY "+column+" ASC";

        if(tipoColeccion == 'parcial'){
          cadena = column+" != 'null'"+conditions+" AND coleccion_usuario.cantidad > 0 GROUP BY "+column+" ORDER BY "+column+" ASC";

          basedatos.callBd("J", [[column], []], [tabla, "coleccion_usuario"], {type:["I"], join:[["Id"],["id_sello"]], where: cadena}).then(
                   //basedatos.callBd("S", [column], tabla, cadena ).then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            console.log(dato)
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        }else{
          basedatos.callBd("S", [column], tabla, cadena ).then(
          function(res){
            //console.log(res);
            for (var i=0; i<res.rows.length; i++){
              dato.push(res.rows.item(i));
            }
            console.log(dato)
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );
        }

        return defferer.promise;
      },

      getTranslatedHeaders : function(condition){
        var defferer = $q.defer();
        var dato = [];
        basedatos.callBd("S", ["*"] , 'headers', condition ).then(
          function(res){
            //console.log(res);
            var jsonObj = res.rows.item(0);
            var keys = Object.keys(jsonObj);
            console.log(keys);
            for ( var i = 1; i<keys.length;i++ ){
              var obj = {};
              obj.cid = keys[i];
              obj.name = jsonObj[keys[i]];
              dato.push(obj);
            }
            defferer.resolve(dato);
          },
          function(error){
            //console.log(error);
            defferer.reject(error);
          }
          );

        return defferer.promise;
      },
      /*
        Actualiza el valor del campo 'Coleccion' en la tabla de sellos maestra. Indica el total de sellos que tiene
        el usuario del selo IdSello
        fields       {object}    {id: 1, nombre: "A"}    Datos a insertar o actualizar estableciendo el nombre del campo como clave y el valor como valor de dicha clave
   * table        {string}    "prueba"
   */
   setColeccionUsuario : function(IdSello, valor){
    var defferer = $q.defer();
    var dato = {'IdSello': '', 'Estado': ''};
    console.log(IdSello + ' / ' + valor);
    basedatos.callBd("R", {'id_usuario':'1','id_sello':IdSello,'cantidad': valor}, 'coleccion_usuario').then(
      function(res){
        console.log(res);
        dato.Idsello = IdSello;
        dato.Estado = 'Ok';
        defferer.resolve(dato);
      },
      function(error){
        console.log(error);
        defferer.reject(error);
      }
      );
  },

  getTablesOfDataBase:function(){
    var defferer = $q.defer();
    var tableNames = [];
    basedatos.callBd("INFO").then(
      function(res){
        console.log(res);
        for (var i=0; i<res.rows.length; i++){
          tableNames.push(res.rows.item(i));
        }
        console.log(tableNames);
        defferer.resolve(tableNames);
      },
      function(error){
        console.log(error);
        defferer.reject(error);
      }
      );
    return defferer.promise;
  }

};

}]);

})();
