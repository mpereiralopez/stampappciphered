'use strict';

(function(){
var app = angular.module('app.controladorGeneral.controller', []);

  app.controller('generalController', function($scope, $location, $translate, $q, datos, modales, basedatos, consultasbd, ENUMERADOS, $uibModal){

    console.log('generalController');

    /* TIPOS ENUMERADOS */
    var tiposApp = ENUMERADOS;
    var defferer = $q.defer();

    /* Inicializaciones del controlador */

    //Objeto modelo de datos
    var main = this;

    /**************************************************************************
     * INICIALIZACIÓN DE DATOS
     *************************************************************************/
     /*Inicialización de país para la app */
     main.banderaColeccion =  'flag-icon-gb';
     main.idiomaColeccion = 'COLECCIONES.cn_CN';
     main.banderaTextos = 'flag-icon-gb';

     //carga el idioma inicialmente para los otros filtros
     datos.setLocalStorage('preferredLanguage','en-us');

     var idiomaText = datos.getLocalStorage("LANG_TEXT");
     console.log(idiomaText);
     if(!idiomaText)idiomaText="IDIOMAS.ENG";

      main.preferredLanguage = datos.getLocalStorage("preferredLanguage");
     if(!main.preferredLanguage)main.preferredLanguage="en-us";

      datos.setLocalStorage('preferredLanguage','es-es');

     $translate.use(main.preferredLanguage);


     main.idiomaTextos = idiomaText;

     /* Inicialización de colección */
     main.sellosColeccion = 0;
     main.sellosUsuario = 0;

     /* MODELO DE DATOS */
    //Control de la cabecera principal, valores iniciales {true, false} -> {perfil, flecha}
    main.datos = datos.getCabecera();
    main.visibilidadCabecera = datos.getVisibilidadCabecera();
    //main.spinner = false;
    main.spinner = datos.getSpinner();

    // main.quitarSpinner = function(){
    //   console.log(main.spinner);
    //   //main.spinner = !main.spinner;
    //   datos.hideSpinner();
    //   main.spinner = false;
    // };

    // $scope.$watch('main.spinner', function(current, previous){
    //   console.log('cambio -->' + current);
    // }, true);

    $scope.$watch(function(){
      return datos.getSpinner();
    }, function(current, previous){
      //console.log('cambio2 -->' + current);
      main.spinner = current;
    }, true);

    // main.mostrarSpinner = function(){
    //   console.log('action1 -> ' + main.spinner);
    //   //main.spinner = !main.spinner;
    //   datos.showSpinner();
    //   main.spinner = true;
    // };

    // $scope.$watch('datos.getSpinner()', function(current, previous){
    //   console.log(current);
    //   main.spinner = current;
    // },true);
    //
    // main.watchSource = function(){
    //   return datos.getSpinner();
    // };

    // $scope.$watch('filtrosAnios.radioModel', function(newValue, oldValue){
    //   filtrosAnios.radioModel = newValue;
    //   filtrosAnios.rangoAnios = parseInt(filtrosAnios.totalAnios / filtrosAnios.radioModel);
    //   filtrosAnios.rangoAniosFin = filtrosAnios.totalAnios % filtrosAnios.radioModel;
    //   //filtrosAnios.getRango(filtrosAnios.rangoAnios);
    //   // filtrosAnios.getSellosRango();
    //   filtrosAnios.anioRangoMostrar = datos.getVectAnio(newValue);
    // });

    //Año de inicio y fin de la colección
    main.fechaInicio = datos.getFechas().inicio;
    main.fechaFin = datos.getFechas().fin;

    /* DATOS PARA EL GO */
    //Ventana que se visualiza actualmente. Permite hacer back
    main.ventanaActual = tiposApp.rutas.home;
    //Tipo colección, si es del usuario o la colección completa
    main.tipoColeccion = '';
    //Texto de la ventana actual
    main.tituloVentana = 'STAMPAPP';

    /* IDIOMA */
    //Texto según el idioma de la ventanta actual
    main.idioma = $translate.preferredLanguage();

    main.cambiarIdioma = function (size, idioma) {

      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'modulos/modales/html/modalSeleccionIdioma.html',
        controller: 'selectorIdiomaController',
        size: size,
        resolve: {
          items: function () {
            return main.idiomaTextos;
          }
        }
      });



      modalInstance.result.then(function (selectedItem) {
        //Parte en caso de ok()
        console.log('Cerrando modal');
        console.log('->' + selectedItem);
        if(selectedItem.jsonLangEntry !== undefined){
         main.idiomaTextos = selectedItem.jsonLangEntry;
         datos.setLocalStorage('LANG_TEXT',selectedItem.jsonLangEntry);
         datos.setLocalStorage('preferredLanguage',selectedItem.jsonLang);
        }
      },function () {
        //Parte en caso de cancel()
        console.log('Cancel');
      });

    };

    /**************************************************************************
     * FUNCIONES DE CONTROL
     *************************************************************************/

    /**
     * [Navegación directa entre vistas]
     * @param  {string} ruta Ventana destino de salto
     * @param  {string} coleccion. Indica si la colección es completa o parcial (la del usuario)
     * @param  {array} parametros. Vector de parámetros que se pasarán al controlador como $routeParams
     * @return {void}
     */
    main.go = function(ruta, coleccion, parametros){

      console.log('Saltar a -> ' + ruta + ' ' + coleccion);
      console.log('Ventana antes del salto -> ' + main.ventanaActual);
      console.log('Parámetros -> ' + parametros);

      var cambioModo;

      main.tipoColeccion = coleccion;
      console.log(tiposApp.rutas);
      if(ruta !== tiposApp.rutas.home){
        datos.setCabecera(false, true);
      }else{
        datos.setCabecera(true, false);
      }

      if(datos.getModo().modoActual !== coleccion){
        console.log('Cambio de modo: ' + coleccion);
        cambioModo = true;
      }else{
        console.log('Misma coleccion: ' + coleccion);
        //se va el cambio cuando es parcial y total cuando pasa el menu de sellos al repetirse
        if(ruta=="/filtroAnios" || "/filtroCategorias"  || "/filtroSeries" || "filtros"){
          //Mantiene el true al entrar en filtros de mis sellos al cambiar entre colección y mis sellos
          cambioModo =  datos.getModo().cambioModo;
        } else{
            cambioModo = false;
        }
      }

      datos.setModo(coleccion, cambioModo);

      //Cambio de título de ventana
      switch (ruta) {
        case tiposApp.rutas.sellos:
          if(coleccion === tiposApp.colecciones.completa){
            main.tituloVentana = 'CATALOGO';
          } else {
            main.tituloVentana = 'MISSELLOS';
          }
          break;
        case tiposApp.rutas.filtroAnios:
          if(coleccion === tiposApp.colecciones.completa){
            main.tituloVentana = 'CATALOGOPORANIOS';
          } else {
            main.tituloVentana = 'MISSELLOSPORANIOS';
          }
          break;
        case tiposApp.rutas.resultadoFiltro:
          if(coleccion === tiposApp.colecciones.completa){
            main.tituloVentana = 'CATALOGOPORANIOS';
          } else {
            main.tituloVentana = 'MISSELLOSPORANIOS';
          }
          break;
        case tiposApp.rutas.filtroCategorias:
          if(coleccion === tiposApp.colecciones.completa){
            main.tituloVentana = 'CATALOGOPORCATEGORIA';
          } else {
            main.tituloVentana = 'MISSELLOSPORCATEGORIA';
          }
          break;
        case tiposApp.rutas.filtroSeries:
          if(coleccion === tiposApp.colecciones.completa){
            main.tituloVentana = 'CATALOGOPORSERIE';
          } else {
            main.tituloVentana = 'MISSELLOSPORSERIE';
          }
          break;
        case tiposApp.rutas.filtros:
          if(coleccion === tiposApp.colecciones.completa){
            main.tituloVentana = 'CATALOGOPORFILTRO';
          } else {
            main.tituloVentana = 'MISSELLOSPORFILTRO';
          }
          break;
        case tiposApp.rutas.noticias:
            main.tituloVentana = 'NOVEDADES';
          break;
        case tiposApp.rutas.poi:
              main.tituloVentana = 'POI';
            break;
        case tiposApp.rutas.shop:
            main.tituloVentana = 'TIENDA';
        break;
        default:
          console.log('Error en la ');
      }

      //Se fija la ventana actual para la vuelta
      main.ventanaActual = ruta;

      //Se realiza el salto
      if(parametros === undefined){
        console.log('no parametros');
        $location.path(ruta);
      } else {
        console.log('parametros');
        var rutai = '';
        for(var i = 0; parametros.length > i; i++){
          rutai = rutai + '/' + parametros[i];
          console.log(rutai);
        }
        console.log(ruta + rutai);
        $location.path(ruta + rutai);
      }
      datos.setLocalStorage('lastVisited',main.ventanaActual);
      console.log('Ventana después del salto -> ' + main.ventanaActual);

    };//End main.go

    /**
     * Navegación inversa entre vistas
     * @return {void}
     */
    main.go2 = function(){

      console.log('Saltar desde -> ' + main.ventanaActual);

      switch (main.ventanaActual) {
        case tiposApp.rutas.sellos:
            main.ventanaActual = tiposApp.rutas.home;
            datos.setCabecera(true, false);
            main.tituloVentana = 'STAMPAPP';
            $location.path(tiposApp.rutas.home);
            console.log('Ventana después del salto -> ' + main.ventanaActual);
            break;
        case tiposApp.rutas.filtros:
            main.ventanaActual = tiposApp.rutas.sellos;
            datos.setCabecera(false, true);
            if(main.tipoColeccion === tiposApp.colecciones.completa){
              main.tituloVentana = 'CATALOGO';
            } else{
              main.tituloVentana = 'MISSELLOS';
            }
            $location.path(tiposApp.rutas.sellos);
            console.log('Ventana después del salto -> ' + main.ventanaActual);
            break;
        case tiposApp.rutas.filtroAnios:
            main.ventanaActual = tiposApp.rutas.sellos;
            datos.setCabecera(false, true);
            if(main.tipoColeccion === tiposApp.colecciones.completa){
              main.tituloVentana = 'CATALOGO';
            } else{
              main.tituloVentana = 'MISSELLOS';
            }
            $location.path(tiposApp.rutas.sellos);
            console.log('Ventana después del salto -> ' + main.ventanaActual);
            break;
        case tiposApp.rutas.resultadoFiltro:
            var navStatus = datos.getNavStatus();
            //al volver atras no tiene que recargar los años
            datos.setModo(datos.getModo().modoActual, "false");
            if(navStatus.id=='freetext'){
              main.ventanaActual = tiposApp.rutas.searchby;
            }else{
              main.ventanaActual = tiposApp.rutas.filtroAnios;
            }

            datos.setCabecera(false, true);
            $location.path(main.ventanaActual);
            console.log('Ventana después del salto -> ' + main.ventanaActual);
            break;
        case tiposApp.rutas.filtroCategorias:
            main.ventanaActual = tiposApp.rutas.sellos;
            datos.setCabecera(false, true);
            if(main.tipoColeccion === tiposApp.colecciones.completa){
              main.tituloVentana = 'CATALOGO';
            } else{
              main.tituloVentana = 'MISSELLOS';
            }
            $location.path(tiposApp.rutas.sellos);
            console.log('Ventana después del salto -> ' + main.ventanaActual);
            break;
        case tiposApp.rutas.filtroSeries:
            main.ventanaActual = tiposApp.rutas.sellos;
            datos.setCabecera(false, true);
            if(main.tipoColeccion === tiposApp.colecciones.completa){
              main.tituloVentana = 'CATALOGO';
            } else{
              main.tituloVentana = 'MISSELLOS';
            }
            $location.path(tiposApp.rutas.sellos);
            console.log('Ventana después del salto -> ' + main.ventanaActual);
            break;
        case tiposApp.rutas.noticias:
             main.ventanaActual = tiposApp.rutas.home;
            main.tituloVentana = 'STAMPAPP';
            datos.setCabecera(true, false);
            $location.path(tiposApp.rutas.home);
            console.log('Ventana después del salto -> ' + main.ventanaActual);
            break;
        case tiposApp.rutas.searchby:
            main.ventanaActual = tiposApp.rutas.home;
            main.tituloVentana = 'STAMPAPP';
            //main.ventanaActual = tiposApp.rutas.searchby;
            datos.setCabecera(true, false);
            $location.path(tiposApp.rutas.home);
            console.log('Ventana después del salto -> ' + main.ventanaActual);
            break;

        case tiposApp.rutas.shopcart:
          main.ventanaActual = datos.getLocalStorage('lastVisited');
          console.log(main.ventanaActual);
          if(main.ventanaActual === undefined){main.ventanaActual=tiposApp.rutas.home;datos.setCabecera(true, false);}
          main.tituloVentana = 'STAMPAPP';
          $location.path(main.ventanaActual);
          console.log('Ventana después del salto -> ' + main.ventanaActual);
          break;

          case tiposApp.rutas.shop:
            main.ventanaActual = tiposApp.rutas.home;
            main.tituloVentana = 'STAMPAPP';
            //main.ventanaActual = tiposApp.rutas.searchby;
            datos.setCabecera(true, false);
            $location.path(tiposApp.rutas.home);
            console.log('Ventana después del salto -> ' + main.ventanaActual);
            break;

            case tiposApp.rutas.poi:
              main.ventanaActual = tiposApp.rutas.shop;
              main.tituloVentana = 'STAMPAPP';
              //main.ventanaActual = tiposApp.rutas.searchby;
              datos.setCabecera(true, false);
              $location.path(tiposApp.rutas.home);
              console.log('Ventana después del salto -> ' + main.ventanaActual);
              break;



        default:
            console.log(main.textosApp.error.navError + ': ' + main.ventanaActual + '???' + tiposApp.rutas.filtroAnios);
      }

    };//End main.go2

    main.backControl = function(){
      alert("WOLOLO");
    }

  });

})();
