/**
 * Módulo principal de la aplicación. Contiene la configuración del routing , crossdomain y timeout.
 * @module StampApp
 */

'use strict';

(function(){
var app = angular.module('StampApp', ['ngRoute',
                                      'ngSanitize',
                                      //'routeStyles',
                                      'ui.bootstrap',
                                      'ngCordova',
                                      'ngCordovaOauth',
                                      'ngAnimate',
                                      'pascalprecht.translate',
                                      'twitter',
                                      'infinite-scroll',
                                      'servicios',
                                      'directivas',
                                      'app.controladorGeneral.controller',
                                      'app.init.controller',
                                      'app.home.controller',
                                      'app.sellos.controller',
                                      'app.filtrosAnios.controller',
                                      'app.filtroCategorias.controller',
                                      'app.filtroSeries.controller',
                                      'app.filtros.controller',
                                      'app.resultadoFiltro.controller',
                                      'app.modales.controller',
                                      'app.searchby.controller',
                                      'app.shopcart.controller',
                                      'app.news.controller',
                                      'app.shop.controller',
                                      'app.poi.controller']);

app.
config(function($routeProvider, $httpProvider, $translateProvider, $locationProvider) {
    //console.log('Iniciando angular config ...');
    /* Routing */
    $routeProvider
            .when('/', {
                controller: 'initController as mainInit',
                templateUrl: 'modulos/init/html/init.html'
            })
            .when('/home', {
                controller: 'homeController',
                templateUrl: 'modulos/home/html/home.html'
            })
            .when('/sellos', {
                controller: 'sellosController',
                templateUrl: 'modulos/sellos/html/sellos.html'
            })
            .when('/filtroAnios', {
                controller: 'filtrosAniosController as filtrosAnios',
                templateUrl: 'modulos/filtroAnios/html/filtroAnios.html'
            })
            .when('/filtroCategorias', {
                controller: 'filtroCategoriasController as filtroCategorias',
                templateUrl: 'modulos/filtroCategorias/html/filtroCategorias.html'
            })
            .when('/filtroSeries', {
                controller: 'filtroSeriesController as filtroSeries',
                templateUrl: 'modulos/filtroSeries/html/filtroSeries.html'
            })
            .when('/filtros', {
                controller: 'filtrosController as dynamicFilters',
                templateUrl: 'modulos/filtros/html/filtros.html'
            })
            .when('/resultadoFiltro/:rango/:anios/:posicion/:type/:text', {
                controller: 'resultadoFiltroController as resultadoFiltro',
                templateUrl: 'modulos/resultadoFiltro/html/resultadoFiltro.html'
            })
            .when('/searchby', {
                controller: 'searchbyController as searchby',
                templateUrl: 'modulos/searchBy/html/searchBy.html'
            })
            .when('/news', {
                controller: 'newsController as news',
                templateUrl: 'modulos/news/html/news.html'
            })
            .when('/shopcart', {
                controller: 'shopcartController as shopcart',
                templateUrl: 'modulos/shopcart/html/shopcart.html'
            })
            .when('/shop', {
                controller: 'shopController as shop',
                templateUrl: 'modulos/shop/html/shop.html'
            })
            .when('/poi', {
                controller: 'POIController as poi',
                templateUrl: 'modulos/poi/html/poi.html'
            })
            .otherwise({ redirectTo: '/home'});

    //Enable cross domain calls
    $httpProvider.defaults.useXDomain = true;
    //$httpProvider.defaults.timeout = 10000;

    $translateProvider.useStaticFilesLoader({
      prefix: 'comun/languages/',
      suffix: '.json'
    });

    $translateProvider.useSanitizeValueStrategy('escaped');
    $translateProvider.useMessageFormatInterpolation();
    $translateProvider.preferredLanguage('zn');
    //$translateProvider.fallbackLanguage('es_ES');
    //console.log('Fin de angular config ...');
}).
//run(function ($window, $rootScope, datosAplicacion){
run(function ($window, $rootScope,$filter){
    //console.log('Iniciando angular run ...');

    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(false);
    }
    if (window.StatusBar) {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
    }


    document.addEventListener("deviceready", function() {

         document.addEventListener("backbutton", function(event) {
            //alert("Backbutton !!");
            event.preventDefault();
            navigator.notification.confirm(
                $filter('translate')('DIALOGOS.SALIR'), // message
                function (buttonIndex){
                    //1 == YES
                    //2 == NO
                    //0 == backbutton
                    //alert("Que se pulsa??: " + buttonIndex);
                    if(buttonIndex == 1){
                        //alert("SALIR !!");
                        navigator.app.exitApp();
                    } //else{
                        //alert("NO SALIR !!");
                    //}
                }, // callback
                $filter('translate')('STAMPAPP'), // title
                [$filter('translate')('DIALOGOS.CONFIRM'),$filter('translate')('DIALOGOS.CANCEL')] // buttonName
            );
        });
    })





    //Activar / Desactivar consola
    var debugMode = true;

    if(!debugMode){
      console.log = function() {}
    }

    /* Control del estado de la conexión a internet */
    //Evita que al llamar a un InnAppBrowser, se quede en estado offline el icono asociado
    //$rootScope.online = true;
    $window.addEventListener("offline", function () {
        $rootScope.$apply(function() {
            //$rootScope.online = false;
            //datosAplicacion.setEstadoConexion(false);
            sessionStorage.setItem('onLine', false);
            //console.log("Evento offline");
        });
    }, false);
    $window.addEventListener("online", function () {
        $rootScope.$apply(function() {
            //$rootScope.online = true;
            //datosAplicacion.setEstadoConexion(true);
            sessionStorage.setItem('onLine', true);
            //console.log("Evento online");
        });
    }, false);


    //console.log('Fin de angular run ...');
});

//app.value('THROTTLE_MILLISECONDS', 2500);

})();
