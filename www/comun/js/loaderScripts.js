head.load(
  /* CORDOVA */
  { file: 'cordova.js' },
  /* JQUERY */
  //{ file: 'comun/libs/jQuery/jquery-1.12.0.min.js' },
  { file: 'comun/libs/jQuery/jquery-3.1.1.min.js' },
  /* ANGULARJS */
  { file: 'comun/libs/angular-1.5.0/angular.min.js' },
  { file: 'comun/libs/angular-1.5.0/angular-resource.min.js' },
  { file: 'comun/libs/angular-1.5.0/angular-route.min.js' },
  { file: 'comun/libs/angular-1.5.0/angular-animate.min.js' },
  { file: 'comun/libs/angular-1.5.0/angular-sanitize.min.js' },
  /* TRADUCCIONES */
  { file: 'comun/libs/Angular_added/angular-translate.min.js' },
  { file: 'comun/libs/Angular_added/angular-translate-loader-static-files.min.js' },
  /* TRADUCCIONES CON PLURALES */
  { file: 'comun/libs/messageformat/messageformat.js' },
  { file: 'comun/libs/angular-translate-interpolation-messageformat/angular-translate-interpolation-messageformat.min.js' },
  //{ file: 'comun/libs/Angular_added/translate-cloak.js' },
  /* CARGAR ESTILOS EN EL ROUTING DEL CONFIG */
  //{ file: 'comun/libs/Angular_added/route-styles.js' },
  /* NG-CORDOVA */
  { file: 'comun/libs/Angular_added/ng-cordova.min.js' },
  { file: 'comun/libs/Angular_added/ng-cordova-oauth.min.js' },

  /* CRIPTOGRAFÍA */
  // { file: 'comun/libs/Angular_added/criptografia/aes.js' },
  // { file: 'comun/libs/Angular_added/criptografia/mdo-angular-cryptography.js' },
  /* BOOTSTRAPJS */
  { file: 'comun/libs/Bootstrap/js/bootstrap.min.js' },
  /* UI-BOOTSTRAPJS */
  { file: 'comun/libs/Angular_added/ui-bootstrap-tpls-0.14.3.min.js' },
  /* NG-INFINITE-SCROLL */
  { file: 'comun/libs/InfiniteScroll/ng-infinite-scroll.min.js' },
  /* APP */
  { file: 'modulos/init/js/initController.js' },
  { file: 'modulos/home/js/homeController.js' },
  { file: 'modulos/sellos/js/sellosController.js' },
  { file: 'modulos/filtros/js/filtrosController.js' },
  { file: 'modulos/filtroAnios/js/filtroAniosController.js' },
  { file: 'modulos/filtroCategorias/js/filtroCategoriasController.js' },
  { file: 'modulos/filtroSeries/js/filtroSeriesController.js' },
  { file: 'modulos/resultadoFiltro/js/resultadoFiltro.js' },
  { file: 'modulos/news/js/news.js' },
  { file: 'modulos/searchBy/js/searchBy.js' },
  { file: 'modulos/shopcart/js/shopcart.js' },
  { file: 'modulos/shop/js/shopController.js' },
  { file: 'modulos/poi/js/poi.js' },

  /* Modales */
  { file: 'modulos/modales/modalesController.js' },
  /*  */
  { file: 'comun/js/twitterFactory.js' },
  { file: 'comun/js/directivas.js' },
  { file: 'comun/js/servicios.js' },
  { file: 'comun/js/generalController.js' },
  { file: 'comun/js/index.js' },
  { file: 'comun/js/app.js' }
);
