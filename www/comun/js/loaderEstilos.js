head.load(
  /* BOOTSTRAP */
  { file: 'comun/libs/Bootstrap/css/bootstrap.min.css' },
  { file: 'comun/libs/Bootstrap/css/bootstrap-theme.min.css' },
  /* FONT-AWESOME */
  { file: 'comun/libs/font-awesome-4.6.3/css/font-awesome.min.css' },
  /* FUENTES DE LETRAS */
  { file: 'comun/libs/fonts/fonts.css' },
  /* ICONOS BANDERAS */
  { file: 'comun/libs/flag-icon-css/css/flag-icon.min.css' },
  /* MODALES */
  { file: 'modulos/modales/css/modalInfoSello.css' },
  /* GENERAL */
  { file: 'comun/css/animate.css' },
  { file: 'comun/css/general.css' },
  { file: 'comun/css/bootstrap-social.css' },
  { file: 'modulos/init/css/init.css' },
  { file: 'modulos/home/css/home.css' },
  { file: 'modulos/sellos/css/sellos.css' },
  { file: 'modulos/filtroAnios/css/filtroAnios.css' },
  { file: 'modulos/filtroCategorias/css/filtroCategorias.css' },
  { file: 'modulos/filtroSeries/css/filtroSeries.css' },
  { file: 'modulos/searchBy/css/searchBy.css' },
  { file: 'modulos/news/css/news.css' },
  { file: 'modulos/resultadoFiltro/css/resultadoFiltro.css' }
);
