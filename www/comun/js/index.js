'use strict';

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener("pause", this.onPause, false);
        var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
        //Lanzar la app con configuración movil o navegador
        if ( app ) {
            // Cordova application
            //console.log("Cargando app en movil");
            sessionStorage.setItem('dispositivo','movil');
        } else {
            // Web page
            //console.log("Cargando app en navegador");
            sessionStorage.setItem('dispositivo','navegador');
            this.onDeviceReady();
        }
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        //console.log("Cordova ready!!");
        //
        AndroidFullScreen.immersiveMode(
          function(){
            console.log('Teclado + SoftKeys ocultadas');
          },
          function(error){
            console.log('Error al ocultar el teclado + softkeys');
          }
        );

        // AndroidFullScreen.showUnderSystemUI(
        //   function(){
        //     console.log('Teclado + SoftKeys ocultadas2');
        //   },
        //   function(error){
        //     console.log('Error al ocultar el teclado + softkeys2');
        //   }
        // );

        window.plugins.sqlDB.copy("stampapp.db", 0,
          function(){ // Correcto copia
            console.log("OK copiada");

            //Lanzando manualmente angular, angular ready
            angular.element(document).ready(function() {
              angular.bootstrap(document, ["StampApp"]);
            });
          },
          function(){ // Error copia
            console.log("KO copiada");

            //Lanzando manualmente angular, angular ready
            angular.element(document).ready(function() {
              angular.bootstrap(document, ["StampApp"]);
            });
          }
        );

    },
    onPause: function(){
        //console.log("APP en pausa!! ...");
    }
};

app.initialize();
