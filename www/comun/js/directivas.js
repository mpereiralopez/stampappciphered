(function (){

  var app = angular.module('directivas', []);

  app.directive('dcModalGeneral', function(){
    return{
      restrict: 'EA',
      scope:{
        dccadena1: '@',
        dccadena2: '@'
      },
      templateUrl: 'comun/js/templates/modalGeneral.html'
    };
  });

})();
